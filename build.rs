use std::process::Command;
use std::{env, fs::File, io::Write, path::Path};

use glob::glob;

fn get_cpu_counts(f: &mut File) {
    let cpu_count = num_cpus::get();
    let phy_count = num_cpus::get_physical();
    writeln!(f, "pub const CPU_COUNT: u8 = {cpu_count};").unwrap();
    writeln!(f, "pub const CPU_PHY_COUNT: u8 = {phy_count};").unwrap();
}

fn check_battery() {
    if battery::Manager::new()
        .ok()
        .and_then(|m| m.batteries().ok())
        .and_then(|mut b| b.next())
        .is_some()
    {
        println!("cargo:rustc-cfg=HAS_BATTERY");
    }
}

fn check_nvme_temp() {
    if glob("/sys/class/nvme/nvme*/hwmon*/temp*_input").is_ok_and(|mut i| i.next().is_some()) {
        println!("cargo:rustc-cfg=HAS_NVME_TEMP");
    }
}

fn find_root(f: &mut File) {
    if let Ok(output) = Command::new("findmnt")
        .args(["--noheadings", "--output", "SOURCE", "/"])
        .output()
    {
        let output = output.stdout.as_slice();
        let dev = String::from_utf8_lossy(output);
        let dev = dev.trim().trim_end_matches(char::is_numeric).trim_end_matches('p');
        let dev = Path::new(dev)
            .canonicalize()
            .unwrap_or_else(|_| panic!("Failed to canonicalize path '{dev}'"));
        let dev = dev
            .to_str()
            .unwrap_or_else(|| panic!("Failed to convert '{dev:?}' to string"));
        let dev = dev.trim_end_matches('p').trim_start_matches("/dev/");
        writeln!(f, "pub const IO_DEV: &str = \"{dev}\";").unwrap();
    }
}

fn main() {
    let out_dir = env::var("OUT_DIR").expect("No out dir");
    let dest_path = Path::new(&out_dir).join("build_constants.rs");
    let mut f = File::create(dest_path).expect("Could not create file");

    get_cpu_counts(&mut f);
    find_root(&mut f);
    check_battery();
    check_nvme_temp();

    println!("cargo:rerun-if-changed=build.rs");
}
