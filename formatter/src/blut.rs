pub const BLUT: [char; 81] = [
    '\u{2800}', '\u{2840}', '\u{28C0}', '\u{2804}', '\u{2844}', '\u{28C4}', '\u{2824}', '\u{2864}',
    '\u{28E4}', '\u{2802}', '\u{2842}', '\u{28C2}', '\u{2806}', '\u{2846}', '\u{28C6}', '\u{2826}',
    '\u{2866}', '\u{28E6}', '\u{2812}', '\u{2852}', '\u{28D2}', '\u{2816}', '\u{2856}', '\u{28D6}',
    '\u{2836}', '\u{2876}', '\u{28F6}', '\u{2801}', '\u{2841}', '\u{28C1}', '\u{2805}', '\u{2845}',
    '\u{28C5}', '\u{2825}', '\u{2865}', '\u{28E5}', '\u{2803}', '\u{2843}', '\u{28C3}', '\u{2807}',
    '\u{2847}', '\u{28C7}', '\u{2827}', '\u{2867}', '\u{28E7}', '\u{2813}', '\u{2853}', '\u{28D3}',
    '\u{2817}', '\u{2857}', '\u{28D7}', '\u{2837}', '\u{2877}', '\u{28F7}', '\u{2809}', '\u{2849}',
    '\u{28C9}', '\u{280D}', '\u{284D}', '\u{28CD}', '\u{282D}', '\u{286D}', '\u{28ED}', '\u{280B}',
    '\u{284B}', '\u{28CB}', '\u{280F}', '\u{284F}', '\u{28CF}', '\u{282F}', '\u{286F}', '\u{28EF}',
    '\u{281B}', '\u{285B}', '\u{28DB}', '\u{281F}', '\u{285F}', '\u{28DF}', '\u{283F}', '\u{287F}',
    '\u{28FF}',
];
