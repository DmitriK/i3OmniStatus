use colors::srgb::Srgb;

const PALETTE: [Srgb; 121] = [
    Srgb { r: 0, g: 0, b: 0 },
    Srgb { r: 0, g: 0, b: 36 },
    Srgb { r: 0, g: 0, b: 51 },
    Srgb { r: 0, g: 0, b: 66 },
    Srgb { r: 0, g: 0, b: 81 },
    Srgb { r: 2, g: 0, b: 90 },
    Srgb { r: 4, g: 0, b: 99 },
    Srgb { r: 7, g: 0, b: 106 },
    Srgb {
        r: 11,
        g: 0,
        b: 115,
    },
    Srgb {
        r: 14,
        g: 0,
        b: 119,
    },
    Srgb {
        r: 20,
        g: 0,
        b: 123,
    },
    Srgb {
        r: 27,
        g: 0,
        b: 128,
    },
    Srgb {
        r: 33,
        g: 0,
        b: 133,
    },
    Srgb {
        r: 41,
        g: 0,
        b: 137,
    },
    Srgb {
        r: 48,
        g: 0,
        b: 140,
    },
    Srgb {
        r: 55,
        g: 0,
        b: 143,
    },
    Srgb {
        r: 61,
        g: 0,
        b: 146,
    },
    Srgb {
        r: 66,
        g: 0,
        b: 149,
    },
    Srgb {
        r: 72,
        g: 0,
        b: 150,
    },
    Srgb {
        r: 78,
        g: 0,
        b: 151,
    },
    Srgb {
        r: 84,
        g: 0,
        b: 152,
    },
    Srgb {
        r: 91,
        g: 0,
        b: 153,
    },
    Srgb {
        r: 97,
        g: 0,
        b: 155,
    },
    Srgb {
        r: 104,
        g: 0,
        b: 155,
    },
    Srgb {
        r: 110,
        g: 0,
        b: 156,
    },
    Srgb {
        r: 115,
        g: 0,
        b: 157,
    },
    Srgb {
        r: 122,
        g: 0,
        b: 157,
    },
    Srgb {
        r: 128,
        g: 0,
        b: 157,
    },
    Srgb {
        r: 134,
        g: 0,
        b: 157,
    },
    Srgb {
        r: 139,
        g: 0,
        b: 157,
    },
    Srgb {
        r: 146,
        g: 0,
        b: 156,
    },
    Srgb {
        r: 152,
        g: 0,
        b: 155,
    },
    Srgb {
        r: 157,
        g: 0,
        b: 155,
    },
    Srgb {
        r: 162,
        g: 0,
        b: 155,
    },
    Srgb {
        r: 167,
        g: 0,
        b: 154,
    },
    Srgb {
        r: 171,
        g: 0,
        b: 153,
    },
    Srgb {
        r: 175,
        g: 1,
        b: 152,
    },
    Srgb {
        r: 178,
        g: 1,
        b: 151,
    },
    Srgb {
        r: 182,
        g: 2,
        b: 149,
    },
    Srgb {
        r: 185,
        g: 4,
        b: 149,
    },
    Srgb {
        r: 188,
        g: 5,
        b: 147,
    },
    Srgb {
        r: 191,
        g: 6,
        b: 146,
    },
    Srgb {
        r: 193,
        g: 8,
        b: 144,
    },
    Srgb {
        r: 195,
        g: 11,
        b: 142,
    },
    Srgb {
        r: 198,
        g: 13,
        b: 139,
    },
    Srgb {
        r: 201,
        g: 17,
        b: 135,
    },
    Srgb {
        r: 203,
        g: 20,
        b: 132,
    },
    Srgb {
        r: 206,
        g: 23,
        b: 127,
    },
    Srgb {
        r: 208,
        g: 26,
        b: 121,
    },
    Srgb {
        r: 210,
        g: 29,
        b: 116,
    },
    Srgb {
        r: 212,
        g: 33,
        b: 111,
    },
    Srgb {
        r: 214,
        g: 37,
        b: 103,
    },
    Srgb {
        r: 217,
        g: 41,
        b: 97,
    },
    Srgb {
        r: 219,
        g: 46,
        b: 89,
    },
    Srgb {
        r: 221,
        g: 49,
        b: 78,
    },
    Srgb {
        r: 223,
        g: 53,
        b: 66,
    },
    Srgb {
        r: 224,
        g: 56,
        b: 54,
    },
    Srgb {
        r: 226,
        g: 60,
        b: 42,
    },
    Srgb {
        r: 228,
        g: 64,
        b: 30,
    },
    Srgb {
        r: 229,
        g: 68,
        b: 25,
    },
    Srgb {
        r: 231,
        g: 72,
        b: 20,
    },
    Srgb {
        r: 232,
        g: 76,
        b: 16,
    },
    Srgb {
        r: 234,
        g: 78,
        b: 12,
    },
    Srgb {
        r: 235,
        g: 82,
        b: 10,
    },
    Srgb {
        r: 236,
        g: 86,
        b: 8,
    },
    Srgb {
        r: 237,
        g: 90,
        b: 7,
    },
    Srgb {
        r: 238,
        g: 93,
        b: 5,
    },
    Srgb {
        r: 239,
        g: 96,
        b: 4,
    },
    Srgb {
        r: 240,
        g: 100,
        b: 3,
    },
    Srgb {
        r: 241,
        g: 103,
        b: 3,
    },
    Srgb {
        r: 241,
        g: 106,
        b: 2,
    },
    Srgb {
        r: 242,
        g: 109,
        b: 1,
    },
    Srgb {
        r: 243,
        g: 113,
        b: 1,
    },
    Srgb {
        r: 244,
        g: 116,
        b: 0,
    },
    Srgb {
        r: 244,
        g: 120,
        b: 0,
    },
    Srgb {
        r: 245,
        g: 125,
        b: 0,
    },
    Srgb {
        r: 246,
        g: 129,
        b: 0,
    },
    Srgb {
        r: 247,
        g: 133,
        b: 0,
    },
    Srgb {
        r: 248,
        g: 136,
        b: 0,
    },
    Srgb {
        r: 248,
        g: 139,
        b: 0,
    },
    Srgb {
        r: 249,
        g: 142,
        b: 0,
    },
    Srgb {
        r: 249,
        g: 145,
        b: 0,
    },
    Srgb {
        r: 250,
        g: 149,
        b: 0,
    },
    Srgb {
        r: 251,
        g: 154,
        b: 0,
    },
    Srgb {
        r: 252,
        g: 159,
        b: 0,
    },
    Srgb {
        r: 253,
        g: 163,
        b: 0,
    },
    Srgb {
        r: 253,
        g: 168,
        b: 0,
    },
    Srgb {
        r: 253,
        g: 172,
        b: 0,
    },
    Srgb {
        r: 254,
        g: 176,
        b: 0,
    },
    Srgb {
        r: 254,
        g: 179,
        b: 0,
    },
    Srgb {
        r: 254,
        g: 184,
        b: 0,
    },
    Srgb {
        r: 254,
        g: 187,
        b: 0,
    },
    Srgb {
        r: 254,
        g: 191,
        b: 0,
    },
    Srgb {
        r: 254,
        g: 195,
        b: 0,
    },
    Srgb {
        r: 254,
        g: 199,
        b: 0,
    },
    Srgb {
        r: 254,
        g: 202,
        b: 1,
    },
    Srgb {
        r: 254,
        g: 205,
        b: 2,
    },
    Srgb {
        r: 254,
        g: 208,
        b: 5,
    },
    Srgb {
        r: 254,
        g: 212,
        b: 9,
    },
    Srgb {
        r: 254,
        g: 216,
        b: 12,
    },
    Srgb {
        r: 255,
        g: 219,
        b: 15,
    },
    Srgb {
        r: 255,
        g: 221,
        b: 23,
    },
    Srgb {
        r: 255,
        g: 224,
        b: 32,
    },
    Srgb {
        r: 255,
        g: 227,
        b: 39,
    },
    Srgb {
        r: 255,
        g: 229,
        b: 50,
    },
    Srgb {
        r: 255,
        g: 232,
        b: 63,
    },
    Srgb {
        r: 255,
        g: 235,
        b: 75,
    },
    Srgb {
        r: 255,
        g: 238,
        b: 88,
    },
    Srgb {
        r: 255,
        g: 239,
        b: 102,
    },
    Srgb {
        r: 255,
        g: 241,
        b: 116,
    },
    Srgb {
        r: 255,
        g: 242,
        b: 134,
    },
    Srgb {
        r: 255,
        g: 244,
        b: 149,
    },
    Srgb {
        r: 255,
        g: 245,
        b: 164,
    },
    Srgb {
        r: 255,
        g: 247,
        b: 179,
    },
    Srgb {
        r: 255,
        g: 248,
        b: 192,
    },
    Srgb {
        r: 255,
        g: 249,
        b: 203,
    },
    Srgb {
        r: 255,
        g: 251,
        b: 216,
    },
    Srgb {
        r: 255,
        g: 253,
        b: 228,
    },
    Srgb {
        r: 255,
        g: 254,
        b: 239,
    },
    Srgb {
        r: 255,
        g: 255,
        b: 249,
    },
    Srgb {
        r: 255,
        g: 255,
        b: 255,
    },
];

pub fn get_color(val: f32) -> Srgb {
    if val <= 0.0 {
        *PALETTE.first().unwrap()
    } else if val >= 1.0 {
        *PALETTE.last().unwrap()
    } else {
        PALETTE[((PALETTE.len() - 1) as f32 * val).round() as usize]
    }
}
