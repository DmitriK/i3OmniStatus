#![allow(clippy::missing_errors_doc)]
#![allow(clippy::non_ascii_literal)]
#![allow(clippy::must_use_candidate)]
#![allow(clippy::cast_precision_loss)]
#![allow(clippy::cast_possible_truncation)]
#![allow(clippy::cast_sign_loss)]

mod blut;
mod ironbow_soft;

use ironbow_soft as ironbow;
use palette::Srgb;
use std::fmt::{Display, Error, Write};

pub const NONE_COLOR: Srgb<u8> = Srgb::new(186, 194, 222);

pub const GOOD_COLOR: Srgb<u8> = Srgb::new(166, 209, 137);
pub const WARN_COLOR: Srgb<u8> = Srgb::new(238, 212, 159);
pub const BAD_COLOR: Srgb<u8> = Srgb::new(245, 169, 127);
pub const CRIT_COLOR: Srgb<u8> = Srgb::new(237, 135, 150);
pub const BACK_COLOR: Srgb<u8> = Srgb::new(17, 17, 27);

pub const GRAY_COLOR: Srgb<u8> = Srgb::new(128, 135, 162);

pub const DECORATIVE_FONT: &str = "Montserrat";
pub const NORMAL_FONT: &str = "Inter";
pub const NERD_FONT: &str = "Symbols Nerd Font Mono";

#[derive(Clone, Copy)]
pub struct DynamicWarn {
    pub val: i32,
    pub min: i32,
    pub mid: i32,
    pub max: i32,
}

#[derive(Clone, Copy)]
pub enum WarnLevel {
    None,
    Warn,
    Crit,
    Dynamic(DynamicWarn),
}

impl WarnLevel {
    #[inline]
    pub fn color(&self) -> Srgb<u8> {
        match self {
            Self::None | Self::Dynamic(_) => NONE_COLOR,
            Self::Warn => WARN_COLOR,
            Self::Crit => CRIT_COLOR,
        }
    }
}

#[allow(dead_code)]
fn get_4_color(v: u8) -> Srgb<u8> {
    if v <= 31 {
        GRAY_COLOR
    } else if v <= 95 {
        GOOD_COLOR
    } else if v <= 159 {
        WARN_COLOR
    } else if v <= 223 {
        BAD_COLOR
    } else {
        CRIT_COLOR
    }
}

fn get_6_color(v: u8) -> Srgb<u8> {
    if v <= 42 {
        Srgb::new(108, 112, 134)
    } else if v <= 85 {
        Srgb::new(114, 135, 253)
    } else if v <= 128 {
        Srgb::new(203, 166, 247)
    } else if v <= 170 {
        Srgb::new(245, 194, 231)
    } else if v <= 213 {
        Srgb::new(245, 224, 220)
    } else {
        Srgb::new(255, 255, 255)
    }
}

pub fn i3_write_to<W: Write>(
    f: &mut W,
    value: &str,
    icon: char,
    unit: Option<&str>,
    warn: WarnLevel,
    icon_val: Option<u8>,
) -> Result<(), Error> {
    let warn_color = match warn {
        WarnLevel::None => None,
        WarnLevel::Warn | WarnLevel::Crit => Some(warn.color()),
        WarnLevel::Dynamic(d) => {
            if d.val <= d.min {
                None
            } else {
                Some(if d.val <= d.mid {
                    WARN_COLOR
                } else if d.val <= d.max {
                    BAD_COLOR
                } else {
                    CRIT_COLOR
                })
            }
        }
    };

    let icon_clr = get_6_color(icon_val.unwrap_or(0));
    write!(
        f,
        "<span font_family=\"{NERD_FONT}\" foreground=\"#{icon_clr:X}\">{icon}</span>\u{2009}"
    )?;
    write!(
        f,
        "<span font_family=\"{NORMAL_FONT}\" font_features=\"tnum\""
    )?;
    if let Some(clr) = warn_color {
        write!(f, " foreground=\"#{clr:X}\"")?;
    } else {
        write!(f, " foreground=\"#{NONE_COLOR:X}\"")?;
    }
    write!(f, ">{value}</span>")?;
    if let Some(s) = unit {
        write!(f, "<span font_family=\"{NORMAL_FONT}\">\u{2009}{s}</span>")?;
    }
    Ok(())
}

pub struct MultiBar {
    icon_val: u8,
    values: [u8; 4],
    warn: u8,
    crit: u8,
    icon: char,
}

impl MultiBar {
    const NUM_BLOCKS: u8 = 5;
    const DOT_MULT: u8 = 2 * Self::NUM_BLOCKS;

    pub fn new(icon: char, warn: u8, crit: u8) -> Self {
        let warn = (warn / 2).clamp(1, Self::NUM_BLOCKS - 1);
        let crit = ((crit + 1) / 2).clamp(2, Self::NUM_BLOCKS - 1);
        Self {
            icon_val: 255,
            values: [Self::DOT_MULT; 4],
            warn,
            crit,
            icon,
        }
    }
    pub fn update(&mut self, icon_val: u8, bar_vals: &[u8; 4]) -> bool {
        let mut result = false;
        for (y, x) in self.values.iter_mut().zip(bar_vals.iter()) {
            if *y != *x {
                result = true;
                *y = (*x).clamp(0, Self::DOT_MULT);
            }
        }
        if self.icon_val != icon_val {
            result = true;
            self.icon_val = icon_val;
        }
        result
    }
    pub fn write_to<W: Write>(&self, dst: &mut W) {
        // let icon_clr = if self.icon_val <= self.crit {
        //     get_clr_str(self.icon_val, self.warn, self.crit, NONE_COLOR, WARN_COLOR)
        // } else {
        //     get_clr_str(self.icon_val, self.crit, 1.0, WARN_COLOR, CRIT_COLOR)
        // };
        let icon_clr = ironbow::get_color(self.icon_val);

        let warn_index = self.warn - 1;
        let crit_index = self.crit - 1;

        let mut phase = -1;
        write!(
            dst,
            "<span font_family=\"{NERD_FONT}\" color=\"#{icon_clr:X}\">{}</span>[",
            self.icon
        )
        .expect("Ran out of room when writing multibar");

        for c_i in 0..Self::NUM_BLOCKS {
            if c_i <= warn_index && phase < 0 {
                write!(dst, "<span color=\"#{GOOD_COLOR:X}\">")
                    .expect("Ran out of room when writing multibar");
                phase += 1;
            } else if c_i > warn_index && phase < 1 {
                write!(dst, "</span><span color=\"#{WARN_COLOR:X}\">")
                    .expect("Ran out of room when writing multibar");
                phase += 1;
            } else if c_i >= crit_index && phase < 2 {
                write!(dst, "</span><span color=\"#{CRIT_COLOR:X}\">")
                    .expect("Ran out of room when writing multibar");
                phase += 1;
            }
            let val = self
                .values
                .iter()
                .map(|x| (i32::from(*x) - 2 * i32::from(c_i)).clamp(0, 2) as u8)
                .enumerate()
                .fold(0, |a, (i, x)| a + 3_u8.pow(i as u32) * x);
            write!(dst, "{}", blut::BLUT[val as usize])
                .expect("Ran out of room when writing multibar");
        }
        write!(dst, "</span>]").expect("Ran out of room when writing multibar");
    }
}

#[derive(Default)]
pub struct PressureMeter {
    icon_value: u16,
    value: u16,
}

impl PressureMeter {
    const WARN: u16 = 8;
    const CRIT: u16 = 16;

    pub fn update(&mut self, icon_val: u16, val: u16) -> bool {
        let mut result = false;
        if self.icon_value != icon_val {
            self.icon_value = icon_val;
            result |= true;
        }
        if self.value != val {
            self.value = val;
            result |= true;
        }
        result
    }

    pub fn write_to<W: Write>(&self, dst: &mut W) {
        let icon_color = get_6_color(((255 * self.icon_value) / Self::CRIT).clamp(0, 255) as u8);
        let color = if self.value <= 1 {
            NONE_COLOR
        } else if self.value <= Self::WARN {
            WARN_COLOR
        } else if self.value <= Self::CRIT {
            BAD_COLOR
        } else {
            CRIT_COLOR
        };

        let icon = if self.value < (Self::WARN / 2) {
            '\u{f0873}'
        } else if self.value < Self::WARN {
            '\u{f0875}'
        } else if self.value < Self::CRIT {
            '\u{f029a}'
        } else {
            '\u{f0874}'
        };

        write!(
            dst,
            "<span font_family=\"{NERD_FONT}\" color=\"#{icon_color:X}\">{icon}</span>\
             <span font_family=\"{NORMAL_FONT}\" color=\"#{color:X}\" font_features=\"tnum\">{:\u{2007}>2}</span>\
            \u{2009}%",
            // "<span color=\"#{:X}\">{}</span><span color=\"#{:X}\">{:\u{2007}>2}%</span>",
            self.value
        )
        .expect("Ran out of room when writing pressure meter");
    }
}

pub fn round<const P: u32>(v: u32) -> u32 {
    if (v % P) >= (P / 2) {
        (v / P) + 1
    } else {
        v / P
    }
}

pub fn format2digit(v: u32) -> String {
    if v >= 9950 {
        // round to nearest 1000KiB
        let v = round::<1000>(v);
        if v >= 100 {
            format!("{v}")
        } else {
            format!("{v}.")
        }
    } else if v >= 995 {
        // round to nearest 100KiB
        let v = round::<100>(v);
        let whole = v / 10;
        let frac = v % 10;
        format!("{whole}.{frac}")
    } else {
        // round to nearest 10KiB
        let v = round::<10>(v);
        format!(".{v:02}")
    }
}

pub struct HexMeter {
    value: u8,
}

impl HexMeter {
    pub fn set_range_integer(&mut self, val: i32, min: i32, max: i32) -> bool {
        let old_val = self.value;
        if max > min {
            let round_offset = (max - min) / 2;
            self.value = ((255 * val + round_offset) / (max - min)).clamp(0, 255) as u8;
            self.value / 32 != old_val / 32
        } else {
            self.value = 255;
            self.value / 32 != old_val / 32
        }
    }
}

impl Default for HexMeter {
    fn default() -> Self {
        Self { value: u8::MAX }
    }
}

impl Display for HexMeter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let hex_color = get_6_color(self.value);
        let code: u32 = (self.value / 32).into();
        let hex = if code == 0 {
            '\u{f02d9}'
        } else if code == 7 {
            '\u{f02d8}'
        } else {
            char::from_u32(code + 0xf0ac2).unwrap_or('?')
        };
        write!(
            f,
            "<span font_family=\"{NERD_FONT}\" color=\"#{hex_color:X}\">{hex}</span>"
        )
    }
}

pub struct StaticIconMeter<const C: char> {
    value: u8,
}

impl<const C: char> StaticIconMeter<C> {
    pub fn set_range_integer(&mut self, val: i32, min: i32, max: i32) -> bool {
        let round_offset = (max - min) / 2;
        let old_val = self.value;
        self.value = ((255 * val + round_offset) / (max - min)).clamp(0, 255) as u8;
        self.value != old_val
    }
}

impl<const C: char> Default for StaticIconMeter<C> {
    fn default() -> Self {
        Self { value: u8::MAX }
    }
}

impl<const C: char> Display for StaticIconMeter<C> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let icon_color = get_6_color(self.value);
        write!(
            f,
            "<span font_family=\"{NERD_FONT}\" color=\"#{icon_color:X}\">{C}</span>"
        )
    }
}
