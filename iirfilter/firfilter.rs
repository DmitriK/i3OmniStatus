#[derive(Default)]
pub struct SimpleFirFilter<T> {
    history: [T; 2],
    // running_sums: [T; 5],
    running_sum: T,
    index: u8,
}

impl<
        T: std::default::Default
            + std::convert::From<u8>
            + std::marker::Copy
            + std::ops::AddAssign
            + std::ops::SubAssign
            + std::ops::Div<T, Output = T>
            + std::ops::Add<T, Output = T>,
    > SimpleFirFilter<T>
{
    #[must_use]
    pub fn new() -> Self {
        Self {
            history: [T::default(); 2],
            index: 0,
            // running_sums: [T::default(); 5],
            running_sum: T::default(),
        }
    }

    pub fn push(&mut self, new: T) {
        // #[allow(clippy::cast_possible_truncation)]
        // for (i, sum) in self.running_sums.iter_mut().enumerate() {
        //     *sum += new;
        //     let offset = 2_u8.pow(i as u32 + 1);
        //     let target_index = self.index.wrapping_sub(offset) % 32;
        //     *sum -= self.history[target_index as usize];
        // }
        self.running_sum += new;
        self.running_sum -= self.history[self.index as usize];
        self.history[self.index as usize] = new;
        self.index = self.index.wrapping_add(1) % 2;
    }

    pub fn get_last(&self) -> T {
        self.history[(self.index.wrapping_sub(1) % 2) as usize]
    }

    pub fn get_average2(&self) -> T {
        (self.running_sum + 1.into()) / 2.into()
    }
    // pub fn get_average4(&self) -> T {
    //     (self.running_sums[1] + 2.into()) / 4.into()
    // }
    // pub fn get_average8(&self) -> T {
    //     (self.running_sums[2] + 4.into()) / 8.into()
    // }
    // pub fn get_average16(&self) -> T {
    //     (self.running_sums[3] + 8.into()) / 16.into()
    // }
    // pub fn get_average32(&self) -> T {
    //     (self.running_sums[4] + 16.into()) / 32.into()
    // }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn filter_behavior() {
        let mut history = SimpleFirFilter::new();
        for val in 1..=32 {
            history.push(val);
        }
        // assert_eq!(history.running_sums[0], 63);
        // assert_eq!(history.running_sums[1], 122);
        // assert_eq!(history.running_sums[2], 228);
        // assert_eq!(history.running_sums[3], 392);
        // assert_eq!(history.running_sums[4], 528);
    }
}
