#![allow(clippy::unreadable_literal)]

pub mod firfilter;
pub use firfilter::SimpleFirFilter;

use core::ops::{Add, Mul};

// Quick conversions for k-sample moving average
// For IIR in form of y[n] = (1-a)*x[n] + a*y[n-1]:
// a = 1-1/(0.599836673868519*k + 0.396429711351043)
// or simpler: a = 1-2/k
// For IIR in form of y[n] = ((1-a)*x[n] + (1-a)*x[n-1])/2 + a*y[n-1]
// a = 1-1/(0.600305771790498*k - 0.213493058314723)

#[allow(clippy::excessive_precision)]
pub mod time_constants {
    pub const TC2: f32 = 0.507146092126728;
    pub const TC4: f32 = 0.250748300949244;
    pub const TC8: f32 = 0.125086917368125;
    pub const TC16: f32 = 0.062510503897665;
    pub const TC32: f32 = 0.031251291845831;
}

pub struct IirFilter<T> {
    strength: f32,
    last_input: T,
    last_output: T,
}

impl<T: Mul<f32, Output = T> + Add<T, Output = T> + Copy + PartialEq + Default> IirFilter<T> {
    #[must_use]
    pub fn new(strength: f32) -> Self {
        Self {
            strength,
            last_input: T::default(),
            last_output: T::default(),
        }
    }

    pub fn push(&mut self, new_val: T) -> T {
        self.last_output = self.last_input * (self.strength / 2.)
            + new_val * (self.strength / 2.)
            + self.last_output * (1. - self.strength);
        self.last_input = new_val;
        self.last_output
    }

    // pub fn push_with_old(&mut self, new_val: T) -> (T, T) {
    //     let old = self.last_output;
    //     (old, self.update(new_val))
    // }

    pub fn last_output(&self) -> T {
        self.last_output
    }
}

pub type IntFilterTC2<T> = IntegerFilter<T, 2751>;
pub type IntFilterTC4<T> = IntegerFilter<T, 1642>;
pub type IntFilterTC8<T> = IntegerFilter<T, 912>;
pub type IntFilterTC16<T> = IntegerFilter<T, 482>;
pub type IntFilterTC32<T> = IntegerFilter<T, 249>;

pub struct IntegerFilter<T, const STRENGTH: i32> {
    last_input: T,
    last_output: T,
}

impl<T, const STRENGTH: i32> Default for IntegerFilter<T, STRENGTH>
where
    T: fixed::traits::Fixed<Bits = i32>,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<T, const STRENGTH: i32> IntegerFilter<T, STRENGTH>
where
    T: fixed::traits::Fixed<Bits = i32>,
{
    #[must_use]
    pub fn new() -> Self {
        Self {
            last_input: T::from_num(0),
            last_output: T::from_num(0),
        }
    }
    pub fn push(&mut self, new_val: T) -> T {
        let x0: i32 = new_val.to_bits();
        let x1: i32 = self.last_input.to_bits();
        let y0: i32 = self.last_output.to_bits();
        let result = STRENGTH * (x0 + x1 - y0 * 2) / 16384 + y0;
        self.last_output = T::from_bits(result);
        self.last_input = new_val;
        self.last_output
    }

    pub fn last_output(&self) -> T {
        self.last_output
    }
}
