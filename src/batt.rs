#![cfg(HAS_BATTERY)]
#![allow(clippy::non_ascii_literal)]

use crate::blocks::{BlockOutputData, SyncBlockGenerator};
use battery::units::power::watt;
use battery::units::ratio::percent;

#[derive(Default, PartialEq)]
struct Values {
    soc_real: i32,
    soc_fake: f32,
    state: battery::State,
    power: i32,
}

pub struct Block {
    cache: Values,
    manager: Option<battery::Manager>,
    battery: Option<battery::Battery>,
}

impl Block {
    const GOOD_LEVEL: f32 = 60.;

    pub fn new() -> Self {
        let mgr = battery::Manager::new().ok();
        let batt = mgr.as_ref().and_then(|m| {
            m.batteries()
                .ok()
                .and_then(|mut b| b.next())
                .and_then(Result::ok)
        });
        Self {
            cache: Values::default(),
            manager: mgr,
            battery: batt,
        }
    }

    #[allow(clippy::too_many_lines)]
    fn write_output(&self, block: &mut BlockOutputData) {
        let Values {
            soc_fake,
            soc_real,
            power: pow,
            state,
        } = self.cache;
        let state_color = if soc_fake > 50. {
            formatter::GOOD_COLOR.blend(formatter::WARN_COLOR, (100. - soc_fake) / 50.)
        } else {
            formatter::WARN_COLOR.blend(formatter::CRIT_COLOR, 1. - soc_fake / 50.)
        };

        let batt_str = match state {
            // battery::State::Unknown if soc_fake < 100. => {
            //     format!("\u{f582}\u{2009}{:\u{2007}>2}\u{2009}%", soc_fake)
            // }
            battery::State::Empty => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󱃍\u{2009}{:\u{2007}>2}\u{2009}%"
                ),
                state_color, soc_fake
            ),
            battery::State::Full | battery::State::Unknown => {
                format!("󰁹\u{2009}{:\u{2007}>2}\u{2009}%", soc_fake)
            }
            // Levels, charging
            battery::State::Charging if soc_fake > 95. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂅\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>",
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Charging if soc_fake > 85. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂋\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>",
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Charging if soc_fake > 75. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂊\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>",
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Charging if soc_fake > 65. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰢞\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>",
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Charging if soc_fake > 55. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂉\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>",
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Charging if soc_fake > 45. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰢝\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>",
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Charging if soc_fake > 35. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂈\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>",
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Charging if soc_fake > 25. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂇\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>",
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Charging if soc_fake > 15. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂆\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>",
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Charging if soc_fake > 5. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰢜\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>",
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Charging => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰢟\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>",
                ),
                state_color, soc_fake, pow
            ),

            // Levels, discharging
            battery::State::Discharging if soc_fake >= 95. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰁹\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>"
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Discharging if soc_fake >= 85. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂂\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>"
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Discharging if soc_fake >= 75. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂁\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>"
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Discharging if soc_fake >= 65. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂀\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>"
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Discharging if soc_fake >= 55. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰁿\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>"
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Discharging if soc_fake >= 45. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰁾\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>"
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Discharging if soc_fake >= 35. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰁽\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>"
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Discharging if soc_fake >= 25. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰁼\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>"
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Discharging if soc_fake >= 15. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰁻\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>"
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Discharging if soc_fake >= 5. => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰁺\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>"
                ),
                state_color, soc_fake, pow
            ),
            battery::State::Discharging => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂎\u{2009}{:\u{2007}>2}\u{2009}%",
                    "\u{2009}{:\u{2007}>2}\u{2009}W</span>"
                ),
                state_color, soc_fake, pow
            ),
            _ => format!(
                concat!(
                    "<span foreground=\"{:X}\">",
                    "󰂃\u{2009}{:\u{2007}>2}\u{2009}%"
                ),
                formatter::CRIT_COLOR,
                soc_fake
            ),
        };
        block.full_text.clear();
        block.full_text.push_str(&batt_str);
    }
}

impl SyncBlockGenerator for Block {
    fn update(&mut self, block: &mut BlockOutputData) -> bool {
        if let (Some(mgr), Some(mut batt)) = (self.manager.as_ref(), self.battery.as_mut()) {
            mgr.refresh(&mut batt).unwrap();
        }

        let soc = self
            .battery
            .as_ref()
            .map_or(0.0, |b| b.state_of_charge().get::<percent>());
        #[allow(clippy::cast_possible_truncation)]
        let soc_real = soc.round() as i32;
        #[allow(clippy::cast_possible_truncation)]
        let soc_fake = (100. * soc / Self::GOOD_LEVEL).round();

        let state = self
            .battery
            .as_ref()
            .map_or(battery::State::Unknown, battery::Battery::state);
        #[allow(clippy::cast_possible_truncation)]
        let pow = self
            .battery
            .as_ref()
            .map_or(0, |b| b.energy_rate().get::<watt>().round() as i32);

        let new_vals = Values {
            soc_fake,
            soc_real,
            state,
            power: pow,
        };

        if self.cache != new_vals {
            self.cache = new_vals;
            self.write_output(block);
            return true;
        }
        false
    }
}
