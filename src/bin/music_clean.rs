use std::collections::HashSet;
use std::fs;
use std::io;
use std::path::Path;

use i3omnistatus::get_albumart_hash;

fn find_images(dir: &Path, map: &mut HashSet<String>) -> io::Result<()> {
    if dir.is_dir() {
        for entry in fs::read_dir(dir)?.filter_map(Result::ok) {
            let path = entry.path();
            if path.is_dir() {
                let _ = find_images(&path, map);
            } else if matches!(
                path.extension().and_then(|s| s.to_str()),
                Some("jpg" | "png" | "webp" | "avif" | "jxl")
            ) {
                if let Some(hash) = get_albumart_hash(&path.to_string_lossy()) {
                    map.insert(format!("{hash:032x}"));
                }
            }
        }
    }
    Ok(())
}

fn main() {
    let mut music_dir = dirs::home_dir().expect("Home directory not found");
    music_dir.push("Music");
    let mut arts = HashSet::new();
    let _ = find_images(&music_dir, &mut arts);

    let mut cache_path = dirs::cache_dir().expect("Missing cache directory");
    cache_path.push("i3omnistatus");
    cache_path.push("music");
    cache_path.push("thumbnails");

    for file in cache_path
        .read_dir()
        .expect("Failed to read cache directory")
        .filter_map(Result::ok)
        .filter(|f| f.file_type().map(|ft| ft.is_file()).unwrap_or(false))
    {
        if let Some(hash) = file.path().file_stem().map(|f| f.to_string_lossy()) {
            if !arts.contains(hash.as_ref()) {
                print!("Deleting {}...", file.file_name().to_string_lossy());
                match fs::remove_file(file.path()) {
                    Ok(()) => {
                        println!("ok");
                    }
                    Err(e) => {
                        println!("Failed due to {e}");
                    }
                }
            }
        }
    }
}
