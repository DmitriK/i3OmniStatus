#![allow(clippy::non_ascii_literal)]

use palette::{FromColor, Oklch, Srgb};
use rand::{rng, Rng};
use serde::Serializer;
use serde_derive::Serialize;

#[derive(Serialize)]
pub struct BlockOutputData {
    pub full_text: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub short_text: Option<String>,
    #[serde(
        skip_serializing_if = "Option::is_none",
        serialize_with = "serialize_optcolor"
    )]
    pub color: Option<Srgb<u8>>,
    #[serde(
        skip_serializing_if = "Option::is_none",
        serialize_with = "serialize_optcolor"
    )]
    pub background: Option<Srgb<u8>>,
    #[serde(serialize_with = "serialize_color")]
    pub border: Srgb<u8>,
    align: &'static str,
    pub name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    markup: Option<&'static str>,
    border_top: u32,
    border_right: u32,
    border_bottom: u32,
    border_left: u32,
}

#[allow(clippy::trivially_copy_pass_by_ref)]
fn serialize_color<S>(color: &Srgb<u8>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let s = format!("#{color:X}");
    serializer.serialize_str(&s)
}

#[allow(clippy::trivially_copy_pass_by_ref, clippy::ref_option)]
fn serialize_optcolor<S>(color: &Option<Srgb<u8>>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    if let Some(c) = color {
        let s = format!("#{c:X}");
        serializer.serialize_str(&s)
    } else {
        serializer.serialize_none()
    }
}

impl BlockOutputData {
    pub fn new(name: &str, is_pango: bool) -> Self {
        let hue = rng().random_range(0.0..360.0);
        let l = 0.823;
        let c = 0.0964;
        let random_border = Srgb::from_color(Oklch::new(l, c, hue)).into_format();
        Self {
            full_text: name.into(),
            short_text: None,
            color: None,
            background: None,
            // border: None,
            align: "right",
            name: name.into(),
            // instance: None,
            // urgent: None,
            markup: if is_pango { Some("pango") } else { None },
            border: random_border,
            border_top: 0,
            border_right: 0,
            border_bottom: 1,
            border_left: 0,
        }
    }
}

pub trait SyncBlockGenerator {
    fn update(&mut self, block: &mut BlockOutputData) -> bool;
}
