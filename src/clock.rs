#![allow(clippy::non_ascii_literal)]

use crate::blocks::{BlockOutputData, SyncBlockGenerator};
use chrono::{offset::Local, offset::Utc, DateTime, NaiveTime, Timelike};
use esbat::lunar_phase;
use palette::{FromColor, LinSrgb, Mix, Oklab, Srgb};
use parking_lot::Mutex;
use spa::{sunrise_and_set, StdFloatOps, SunriseAndSet};
use std::fmt::Write;
use std::sync::Arc;
use std::thread;
use std::thread::Thread;
use std::time::Duration;

use formatter::{NERD_FONT, NORMAL_FONT};

const LAT: f64 = 41.482_222;
const LNG: f64 = -81.669_722;

const CLR_NOON: Srgb<u8> = Srgb::new(245, 224, 220);
const CLR_DUSK: Srgb<u8> = Srgb::new(245, 194, 231);
const CLR_NITE: Srgb<u8> = Srgb::new(203, 166, 247);
const CLR_DAWN: Srgb<u8> = Srgb::new(242, 205, 205);

fn get_moon_icon(angle: f64) -> char {
    const BASE: u32 = 0xe38d;

    let angle = if angle < 0.0 { angle + 360.0 } else { angle };
    #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
    let offset = (angle * 28.0 / 360.0).round() as u32 % 28;
    char::from_u32(BASE + offset).unwrap_or('\u{f186}')
}

fn get_color_mixed(time: NaiveTime, rise: NaiveTime, set: NaiveTime) -> Srgb<u8> {
    let time = (time - NaiveTime::from_hms(0, 0, 0)).num_seconds();
    let rise = (rise - NaiveTime::from_hms(0, 0, 0)).num_seconds();
    let set = (set - NaiveTime::from_hms(0, 0, 0)).num_seconds();
    let noon = (rise + set) / 2;
    let midnight = noon - 12 * 3600;
    let next_midnight = noon + 12 * 3600;
    let (clr_1, clr_2, time1, time2) = if time < rise {
        (CLR_NITE, CLR_DAWN, midnight, rise)
    } else if time < noon {
        (CLR_DAWN, CLR_NOON, rise, noon)
    } else if time < set {
        (CLR_NOON, CLR_DUSK, noon, set)
    } else {
        (CLR_DUSK, CLR_NITE, set, next_midnight)
    };
    #[allow(clippy::cast_precision_loss)]
    let u = ((time - time1) as f32) / ((time2 - time1) as f32);
    let mix = 3.0 * u.powi(2) - 2.0 * u.powi(3);

    let clr1_lin = LinSrgb::from_color(clr_1.into_format());
    let clr2_lin = LinSrgb::from_color(clr_2.into_format());
    let mixed = clr1_lin.mix(clr2_lin, mix);

    Srgb::from_color(mixed).into_format()
}

fn get_color(cache: &TimeCache, time: DateTime<Utc>) -> Srgb<u8> {
    // let rise_set = calc_sunrise_and_set(time, LAT, LNG);
    let rise_set = cache.rise_set.as_ref();

    match rise_set {
        Some(SunriseAndSet::PolarNight) => CLR_NITE,
        Some(SunriseAndSet::PolarDay) => CLR_NOON,
        Some(SunriseAndSet::Daylight(rise, set)) => get_color_mixed(
            time.with_timezone(&Local).time(),
            rise.with_timezone(&Local).time(),
            set.with_timezone(&Local).time(),
        ),
        None => get_color_mixed(
            time.with_timezone(&Local).time(),
            NaiveTime::from_hms(6, 0, 0),
            NaiveTime::from_hms(18, 0, 0),
        ),
    }
}

pub struct TimeCache {
    time: DateTime<Local>,
    updated: bool,
    sunrise_offset: chrono::Duration,
    rise_set: Option<SunriseAndSet>,
    last_sunrise_calc: DateTime<Local>,
}

impl TimeCache {
    fn new() -> Self {
        let mut res = Self {
            time: Local::now(),
            updated: false,
            sunrise_offset: chrono::Duration::zero(),
            last_sunrise_calc: Local::now(),
            rise_set: None,
        };
        res.update_sunrise_offset();
        res
    }
    fn update_sunrise_offset(&mut self) {
        self.rise_set = sunrise_and_set::<StdFloatOps>(self.time.into(), LAT, LNG).ok();
        if let Some(SunriseAndSet::Daylight(rise, _)) = self.rise_set {
            let rise: DateTime<Local> = rise.into();
            let diff = NaiveTime::from_hms_opt(7, 0, 0).unwrap_or_default() - rise.time();
            self.sunrise_offset = diff;
        } else {
            eprintln!("Unexpectedly failed to get sunrise time");
            self.sunrise_offset = chrono::Duration::zero();
        }
    }
}

fn clock_thread(time_cache: &Arc<Mutex<TimeCache>>, channel: &Arc<Thread>) {
    loop {
        let local = Local::now();
        {
            let mut tc = time_cache.lock();
            tc.time = local;
            tc.updated = true;

            // Update sunrise time on midnight minute
            if local - tc.last_sunrise_calc > chrono::Duration::days(1) {
                tc.update_sunrise_offset();
            }
        }
        channel.unpark();

        let next_min = local + chrono::Duration::minutes(1);
        let next_min = next_min
            .with_second(0)
            .and_then(|x| x.with_nanosecond(0))
            .unwrap_or(next_min);
        let sleep_time = next_min - local;
        thread::sleep(sleep_time.to_std().unwrap_or(Duration::from_secs(1)));
    }
}

pub struct Block {
    time: Arc<Mutex<TimeCache>>,
}

impl Block {
    pub fn new(channel: Arc<Thread>) -> Self {
        let result = Self {
            time: Arc::new(Mutex::new(TimeCache::new())),
        };
        let time_c = result.time.clone();
        thread::Builder::new()
            .name("Clock".into())
            .spawn(move || clock_thread(&time_c, &channel))
            .unwrap();
        result
    }

    pub fn write_output(cache: &TimeCache, block: &mut BlockOutputData) {
        let now = cache.time;
        let now_utc: DateTime<Utc> = now.into();
        let fmt_date = now.format("%b %d");
        let fmt_time = now.format("%H:%M");

        #[allow(clippy::cast_precision_loss)]
        let num_seconds = now.timestamp() as f32;
        let minute_angle = (num_seconds / 3600.0 % 1.0) * 2.0 * std::f32::consts::PI;
        let a = 0.0964 * minute_angle.sin();
        let b = 0.0964 * minute_angle.cos();

        let lab = Oklab::new(0.823, a, b);
        let color = Srgb::from_color(lab).into_format();

        let sunrise_time = now + cache.sunrise_offset;
        let sunrise_fmt = sunrise_time.format("%H:%M");

        block.full_text.clear();
        write!(
            &mut block.full_text,
                "<span font_family=\"{NERD_FONT}\">{}</span>\
                 <span font_family=\"{NORMAL_FONT}\" font_size=\"80%\"> {fmt_date} </span>\
                 <span font_family=\"{NORMAL_FONT}\" font_features=\"tnum\">{fmt_time}</span>\
                 <span font_family=\"{NORMAL_FONT}\" font_features=\"tnum\" font_size=\"80%\"> {sunrise_fmt}</span>",
            get_moon_icon(lunar_phase(now_utc))
        )
        .expect("Failed to write long clock format");

        let s = block.short_text.get_or_insert(String::new());

        s.clear();
        write!(
            s,
            "<span font_family=\"{NORMAL_FONT}\" font_features=\"tnum\" >{fmt_time}</span>",
        )
        .expect("Failed to write short clock format");

        block.color = Some(color);
        block.border = get_color(cache, now_utc);
    }
}

impl SyncBlockGenerator for Block {
    fn update(&mut self, block: &mut BlockOutputData) -> bool {
        let mut time = self.time.lock();
        if time.updated {
            time.updated = false;

            Self::write_output(&time, block);

            return true;
        }
        false
    }
}
