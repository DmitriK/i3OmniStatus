include!(concat!(env!("OUT_DIR"), "/build_constants.rs"));

use std::time::Duration;

pub const UPDATE_PERIOD: Duration = Duration::from_secs(2);
