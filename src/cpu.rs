#![allow(clippy::non_ascii_literal)]
#![allow(clippy::cast_precision_loss)]
#![allow(clippy::similar_names)]

use std::fmt::Write;

use crate::blocks::{BlockOutputData, SyncBlockGenerator};
use crate::constants::CPU_COUNT;
use crate::pressure::{self, Pressure};
use formatter::{i3_write_to, DynamicWarn, HexMeter, PressureMeter, StaticIconMeter, WarnLevel};
use i3omnistatus::{read_int_attr, FileReader};
use iirfilter::SimpleFirFilter;
use std::convert::TryInto;
use std::fs::{read_dir, File};
use std::io::BufReader;
use std::path::PathBuf;

const CPU_ICON_LAPTOP: char = '\u{f0322}';
const CPU_ICON_DESKTOP: char = '\u{f01c5}';

const POWER_CRIT: i32 = 48_000;
const POWER_WARN: i32 = 37_500;

struct JiffyCounts {
    working: u64,
    total: u64,
}

fn get_coretemp_path() -> Option<PathBuf> {
    for entry in read_dir("/sys/devices/pci0000:00/0000:00:18.3/hwmon")
        .ok()?
        .filter_map(Result::ok)
    {
        if entry.file_name().to_str()?.starts_with("hwmon") {
            return Some(entry.path());
        }
    }
    None
}

fn get_temp(buf: &mut BufReader<File>) -> Option<i16> {
    let t: Option<i32> = read_int_attr(buf);
    t.and_then(|t| (t / 1000 + i32::from(t % 1000 >= 500)).try_into().ok())
}

fn get_power(buf: &mut BufReader<File>) -> Option<i32> {
    let p: Option<i32> = read_int_attr(buf);
    p.map(|p| (p / 1000 + i32::from(p % 1000 >= 500)))
}

fn get_jiffies(buf: &mut FileReader) -> Option<JiffyCounts> {
    buf.rewind().ok()?;
    buf.read_line().ok()?;
    let str_buf = buf.get_str();

    let mut jiffies = str_buf.split_whitespace();
    if jiffies.next() != Some("cpu") {
        return None;
    }

    let mut total_jiffies: u64 = 0;
    let mut work_jiffies: u64 = 0;

    for (index, jiffy) in jiffies.enumerate() {
        let jiffy = jiffy.parse::<u64>().ok()?;
        total_jiffies += jiffy;
        if index != 3 {
            work_jiffies += jiffy;
        }
    }

    Some(JiffyCounts {
        working: work_jiffies,
        total: total_jiffies,
    })
}

pub struct Block {
    old_jiffies: Option<JiffyCounts>,
    stat_buffer: Option<FileReader>,
    pressure: Option<Pressure<pressure::Cpu>>,
    pressure_meter: PressureMeter,
    ct_buf: Option<BufReader<File>>,
    power_cor_buf: Option<BufReader<File>>,
    power_soc_buf: Option<BufReader<File>>,
    temp_warn: Option<i16>,
    temp_crit: Option<i16>,
    temp_filt: SimpleFirFilter<i16>,
    power_cor_filt: SimpleFirFilter<i32>,
    power_soc_filt: SimpleFirFilter<i32>,
    #[cfg(HAS_BATTERY)]
    icon_meter: StaticIconMeter<CPU_ICON_LAPTOP>,
    #[cfg(not(HAS_BATTERY))]
    icon_meter: StaticIconMeter<CPU_ICON_DESKTOP>,
    usage_hex: HexMeter,
    usage_hist: SimpleFirFilter<u16>,
}

impl Block {
    pub fn new() -> Self {
        let ct_buf = {
            get_coretemp_path()
                .map(|p| [p, "temp1_input".into()].iter().collect::<PathBuf>())
                .and_then(|p| File::open(p).ok())
                .map(|f| BufReader::with_capacity(8, f))
        };

        let power_cor_buf = {
            get_coretemp_path()
                .map(|p| [p, "power1_input".into()].iter().collect::<PathBuf>())
                .and_then(|p| File::open(p).ok())
                .map(|f| BufReader::with_capacity(16, f))
        };
        let power_soc_buf = {
            get_coretemp_path()
                .map(|p| [p, "power2_input".into()].iter().collect::<PathBuf>())
                .and_then(|p| File::open(p).ok())
                .map(|f| BufReader::with_capacity(16, f))
        };

        let (temp_warn, temp_crit) = (Some(75), Some(90));

        let stat_file = File::open("/proc/stat").ok();

        Self {
            stat_buffer: stat_file.map(|f| FileReader::with_capacities(f, 256, 64)),
            old_jiffies: None,
            pressure: Pressure::new(),
            pressure_meter: PressureMeter::default(),
            ct_buf,
            power_cor_buf,
            power_soc_buf,
            temp_warn,
            temp_crit,
            temp_filt: SimpleFirFilter::default(),
            power_cor_filt: SimpleFirFilter::default(),
            power_soc_filt: SimpleFirFilter::default(),
            icon_meter: StaticIconMeter::default(),
            usage_hex: HexMeter::default(),
            usage_hist: SimpleFirFilter::default(),
        }
    }
    #[allow(clippy::too_many_lines)]
    fn write_output(&self, block: &mut BlockOutputData) {
        block.full_text.clear();
        let out_str = &mut block.full_text;

        let _ = write!(out_str, "{} {} ", self.icon_meter, self.usage_hex);

        self.pressure_meter.write_to(out_str);

        let warn = match (self.temp_warn, self.temp_crit) {
            (Some(warn), Some(crit)) => WarnLevel::Dynamic(DynamicWarn {
                val: self.temp_filt.get_last().into(),
                min: (warn - (crit - warn)).into(),
                mid: (warn).into(),
                max: (crit).into(),
            }),
            (_, Some(crit)) if self.temp_filt.get_average2() >= crit => WarnLevel::Crit,
            (Some(warn), _) if self.temp_filt.get_average2() >= warn => WarnLevel::Warn,
            _ => WarnLevel::None,
        };

        let icon_filtered = self.temp_filt.get_average2();
        let icon = match (self.temp_warn, self.temp_crit) {
            (Some(warn), _) if icon_filtered >= warn => '\u{F2C7}',
            (Some(warn), Some(crit)) if icon_filtered >= warn - (crit - warn) => '\u{F2C8}',
            (Some(warn), Some(crit)) if icon_filtered >= warn - 2 * (crit - warn) => '\u{F2C9}',
            (Some(warn), Some(crit)) if icon_filtered >= warn - 3 * (crit - warn) => '\u{F2CA}',
            _ => '\u{F2CB}',
        };
        let icon_val = match (self.temp_warn, self.temp_crit) {
            (Some(warn), Some(crit)) => {
                let t_lo = warn - (crit - warn);
                let t_hi = warn;
                let t = self.temp_filt.get_average2();
                #[allow(clippy::cast_sign_loss)]
                Some(((255 * (t - t_lo)) / (t_hi - t_lo)).clamp(0, 255) as u8)
            }
            _ => None,
        };

        out_str.push('\u{2009}');
        #[allow(clippy::non_ascii_literal)]
        i3_write_to(
            out_str,
            &format!("{:\u{2007}>2}", self.temp_filt.get_last()),
            icon,
            Some("°"),
            warn,
            icon_val,
        )
        .expect("Failed to write cpu temp");

        if self.power_cor_buf.is_some() {
            let p = self.power_cor_filt.get_average2();
            let warn = WarnLevel::Dynamic(DynamicWarn {
                val: p,
                min: POWER_WARN - (POWER_CRIT - POWER_WARN),
                mid: POWER_WARN,
                max: POWER_CRIT,
            });
            let p_lo = POWER_WARN - (POWER_CRIT - POWER_WARN);
            let p_hi = POWER_WARN;
            #[allow(clippy::cast_sign_loss)]
            let icon_val = ((255 * (self.power_cor_filt.get_average2() - p_lo)) / (p_hi - p_lo))
                .clamp(0, 255) as u8;
            i3_write_to(
                out_str,
                &format!("{:\u{2007}>2}", p / 1000 + i32::from(p % 1000 >= 500)),
                '\u{f140b}',
                Some("W"),
                warn,
                Some(icon_val),
            )
            .expect("Failed to write CPU core power");
        }
        if self.power_soc_buf.is_some() {
            let p = self.power_soc_filt.get_average2();
            let warn = WarnLevel::Dynamic(DynamicWarn {
                val: p,
                min: POWER_WARN - (POWER_CRIT - POWER_WARN),
                mid: POWER_WARN,
                max: POWER_CRIT,
            });
            let p_lo = POWER_WARN - (POWER_CRIT - POWER_WARN);
            let p_hi = POWER_WARN;
            #[allow(clippy::cast_sign_loss)]
            let icon_val = ((255 * (self.power_soc_filt.get_average2() - p_lo)) / (p_hi - p_lo))
                .clamp(0, 255) as u8;
            i3_write_to(
                out_str,
                &format!("{:\u{2007}>2}", p / 1000 + i32::from(p % 1000 >= 500)),
                '\u{f140c}',
                Some("W"),
                warn,
                Some(icon_val),
            )
            .expect("Failed to write CPU soc power");
        }
    }
}

impl SyncBlockGenerator for Block {
    fn update(&mut self, block: &mut BlockOutputData) -> bool {
        let new_jiffies = self.stat_buffer.as_mut().and_then(get_jiffies);

        let usage = self
            .old_jiffies
            .as_ref()
            .and_then(|old| {
                #[allow(clippy::cast_possible_truncation)]
                new_jiffies.as_ref().and_then(|new| {
                    if new.total < old.total {
                        // Total jiffies went backwards. Happens when going to sleep
                        None
                    } else {
                        let working_delta = new.working - old.working;
                        let total_delta = new.total - old.total;
                        // % with basic rounding
                        ((100 * working_delta + total_delta / 2) / total_delta)
                            .try_into()
                            .ok()
                    }
                })
            })
            .unwrap_or(0);
        self.old_jiffies = new_jiffies;

        self.usage_hist.push(usage);

        let mut result = false;
        result |= self.icon_meter.set_range_integer(
            i32::from(CPU_COUNT) * i32::from(self.usage_hist.get_average2()),
            0,
            100,
        );
        result |= self
            .usage_hex
            .set_range_integer(self.usage_hist.get_average2().into(), 0, 100);

        if let Some(p) = self.pressure.as_mut().and_then(Pressure::get) {
            self.pressure_meter.update(p.1.into(), p.0.into());
            result = true;
        }

        let new_temperature = self.ct_buf.as_mut().and_then(get_temp);
        if let Some(t) = new_temperature {
            let old2 = self.temp_filt.get_average2();
            // let old8 = self.temp_filt.get_average4();
            // let old16 = self.temp_filt.get_average8();
            self.temp_filt.push(t);
            let new2 = self.temp_filt.get_average2();
            // let new8 = self.temp_filt.get_average4();
            // let new16 = self.temp_filt.get_average8();
            if new2 != old2 {
                result = true;
            }
        }

        let new_cor_power = self.power_cor_buf.as_mut().and_then(get_power);
        if let Some(p) = new_cor_power {
            let old2 = self.power_cor_filt.get_average2();
            // let old8 = self.power_cor_filt.get_average8();
            self.power_cor_filt.push(p);
            let new2 = self.power_cor_filt.get_average2();
            // let new8 = self.power_cor_filt.get_average8();
            if new2 != old2 {
                result = true;
            }
        }

        let new_soc_power = self.power_soc_buf.as_mut().and_then(get_power);
        if let Some(p) = new_soc_power {
            let old2 = self.power_soc_filt.get_average2();
            // let old8 = self.power_soc_filt.get_average8();
            self.power_soc_filt.push(p);
            let new2 = self.power_soc_filt.get_average2();
            // let new8 = self.power_soc_filt.get_average8();
            if new2 != old2 {
                result = true;
            }
        }

        if result {
            self.write_output(block);
        }

        result
    }
}
