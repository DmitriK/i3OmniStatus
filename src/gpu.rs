#![allow(clippy::non_ascii_literal)]
#![allow(clippy::cast_precision_loss)]
#![allow(clippy::similar_names)]
#![allow(clippy::redundant_closure_for_method_calls)]

use crate::blocks::{BlockOutputData, SyncBlockGenerator};
use formatter::{i3_write_to, DynamicWarn, HexMeter, StaticIconMeter, WarnLevel};
use i3omnistatus::{read_int_attr, FileReader};
use iirfilter::SimpleFirFilter;
use std::convert::TryInto;
use std::fmt::Write;
use std::format;
use std::fs::{read_dir, File};
use std::io::BufReader;
use std::path::{Path, PathBuf};

const GPU_DEV: &str = "/sys/devices/pci0000:00/0000:00:08.1/0000:c4:00.0";
const POWER_CRIT: i32 = 54_000;
const POWER_WARN: i32 = 32_000;

pub struct Block {
    icon_meter: StaticIconMeter<'\u{F0379}'>,
    usg_hex: HexMeter,
    gtt_hex: HexMeter,
    usg_filter: SimpleFirFilter<u16>,
    gtt_filter: SimpleFirFilter<u16>,
    sclk_filter: SimpleFirFilter<u16>,
    temp_warn: Option<i32>,
    temp_crit: Option<i32>,
    temp_filt: SimpleFirFilter<i32>,
    ppt_filt: SimpleFirFilter<i32>,
    usage_buf: Option<BufReader<File>>,
    gtt_bufs: Option<[BufReader<File>; 2]>,
    temp_buf: Option<BufReader<File>>,
    sclk_buf: Option<FileReader>,
    sclk_limits: Option<[u16; 2]>,
    ppt_buf: Option<BufReader<File>>,
}

fn get_hwmon_path() -> Option<PathBuf> {
    read_dir(Path::new(GPU_DEV).join("hwmon"))
        .ok()?
        .filter_map(Result::ok)
        .find_map(|e| {
            if e.file_name().to_str()?.starts_with("hwmon") {
                Some(e.path())
            } else {
                None
            }
        })
}

impl Block {
    pub fn new() -> Self {
        let (temp_warn, temp_crit) = (Some(75), Some(90));

        let (usage_buf, gtt_bufs, temp_buf, ppt_buf) = {
            let f = File::open(Path::new(GPU_DEV).join("gpu_busy_percent"));
            let util_buf = f.map(|f| BufReader::with_capacity(8, f)).ok();

            let f = File::open(Path::new(GPU_DEV).join("mem_info_gtt_used"));
            let gtt_buf1 = f.map(|f| BufReader::with_capacity(16, f)).ok();

            let f = File::open(Path::new(GPU_DEV).join("mem_info_gtt_total"));
            let gtt_buf2 = f.map(|f| BufReader::with_capacity(16, f)).ok();

            let mut path = get_hwmon_path();
            if let Some(p) = path.as_mut() {
                p.push("temp1_input");
            }
            let temp_buf = path
                .and_then(|p| File::open(p).ok())
                .map(|f| BufReader::with_capacity(8, f));

            let mut path = get_hwmon_path();
            if let Some(p) = path.as_mut() {
                p.push("power1_input");
            }
            let ppt_buf = path
                .and_then(|p| File::open(p).ok())
                .map(|f| BufReader::with_capacity(16, f));

            (
                util_buf,
                gtt_buf1.zip(gtt_buf2).map(|(m1, m2)| [m1, m2]),
                temp_buf,
                ppt_buf,
            )
        };

        let f = File::open(Path::new(GPU_DEV).join("pp_dpm_sclk"));
        let mut sclk_buf = f.map(|f| FileReader::with_capacities(f, 128, 64)).ok();

        let get_limits = |buf: &mut FileReader| {
            let mut min: Option<u16> = None;
            let mut max: Option<u16> = None;
            let lines = buf
                .rewind()
                .and_then(|_| buf.read_all())
                .map(|_| buf.get_str());
            if let Ok(lines) = lines {
                for line in lines.lines() {
                    let mut tokens = line.split_whitespace();
                    let f = tokens
                        .nth(1)
                        .and_then(|s| s.strip_suffix("Mhz"))
                        .and_then(|s| s.parse::<u16>().ok());
                    if let Some(f) = f {
                        min = Some(min.map_or(f, |old| old.min(f)));
                        max = Some(max.map_or(f, |old| old.max(f)));
                    }
                }
            }
            min.zip(max).map(|(min, max)| [min, max])
        };

        let sclk_limits = sclk_buf.as_mut().and_then(get_limits);

        Self {
            icon_meter: StaticIconMeter::default(),
            usg_hex: HexMeter::default(),
            gtt_hex: HexMeter::default(),
            temp_warn,
            temp_crit,
            temp_filt: SimpleFirFilter::default(),
            usg_filter: SimpleFirFilter::default(),
            gtt_filter: SimpleFirFilter::default(),
            sclk_filter: SimpleFirFilter::default(),
            ppt_filt: SimpleFirFilter::default(),
            usage_buf,
            gtt_bufs,
            temp_buf,
            sclk_buf,
            sclk_limits,
            ppt_buf,
        }
    }

    fn get_mem(bufs: &mut [BufReader<File>; 2]) -> Option<u8> {
        let used = read_int_attr::<u64>(&mut bufs[0]).map(|x| (x / 1024 / 1024));
        let total = read_int_attr::<u64>(&mut bufs[1]).map(|x| (x / 1024 / 1024));
        used.zip(total)
            .and_then(|(u, t)| ((100 * u + t / 2) / t).try_into().ok())
    }

    fn get_temp(buf: &mut BufReader<File>) -> Option<i32> {
        let t: Option<i32> = read_int_attr(buf);
        t.map(|t| (t / 1000 + i32::from(t % 1000 >= 500)))
    }

    fn get_clk(buf: &mut FileReader) -> Option<u16> {
        buf.rewind().ok()?;
        buf.read_all().ok()?;
        let s = buf.get_str();

        let line = s.lines().find(|&v| v.ends_with('*'));
        line.map(|l| l.split_whitespace())
            .as_mut()
            .and_then(|t| t.nth(1))
            .and_then(|s| s.strip_suffix("Mhz"))
            .and_then(|s| s.parse::<u16>().ok())
    }

    fn write_output(&self, block: &mut BlockOutputData) {
        block.full_text.clear();
        let out_str = &mut block.full_text;

        let _ = write!(
            out_str,
            "{} {}{} ",
            self.icon_meter, self.usg_hex, self.gtt_hex
        );

        let warn = match (self.temp_warn, self.temp_crit) {
            (Some(warn), Some(crit)) => WarnLevel::Dynamic(DynamicWarn {
                val: self.temp_filt.get_last(),
                min: (warn - (crit - warn)),
                mid: warn,
                max: crit,
            }),
            (_, Some(crit)) if self.temp_filt.get_average2() >= crit => WarnLevel::Crit,
            (Some(warn), _) if self.temp_filt.get_average2() >= warn => WarnLevel::Warn,
            _ => WarnLevel::None,
        };

        let icon_filtered = self.temp_filt.get_average2();
        let icon = match (self.temp_warn, self.temp_crit) {
            (Some(warn), _) if icon_filtered >= warn => '\u{F2C7}',
            (Some(warn), Some(crit)) if icon_filtered >= warn - (crit - warn) => '\u{F2C8}',
            (Some(warn), Some(crit)) if icon_filtered >= warn - 2 * (crit - warn) => '\u{F2C9}',
            (Some(warn), Some(crit)) if icon_filtered >= warn - 3 * (crit - warn) => '\u{F2CA}',
            _ => '\u{F2CB}',
        };
        let icon_val = match (self.temp_warn, self.temp_crit) {
            (Some(warn), Some(crit)) => {
                let t = self.temp_filt.get_average2();
                #[allow(clippy::cast_sign_loss)]
                Some(((255 * (t - (warn - (crit - warn)))) / (crit - warn)).clamp(0, 255) as u8)
            }
            _ => None,
        };

        #[allow(clippy::non_ascii_literal)]
        i3_write_to(
            out_str,
            &format!("{:\u{2007}>2}", self.temp_filt.get_last()),
            icon,
            Some("°"),
            warn,
            icon_val,
        )
        .expect("Failed to write gpu temp");

        if self.ppt_buf.is_some() {
            let p = self.ppt_filt.get_average2();
            let warn = WarnLevel::Dynamic(DynamicWarn {
                val: p,
                min: POWER_WARN - (POWER_CRIT - POWER_WARN),
                mid: POWER_WARN,
                max: POWER_CRIT,
            });
            let p_lo = POWER_WARN - (POWER_CRIT - POWER_WARN);
            let p_hi = POWER_WARN;
            #[allow(clippy::cast_sign_loss)]
            let icon_val =
                ((255 * (self.ppt_filt.get_average2() - p_lo)) / (p_hi - p_lo)).clamp(0, 255) as u8;
            i3_write_to(
                out_str,
                &format!("{:\u{2007}>2}", p / 1000 + i32::from(p % 1000 >= 500)),
                '\u{f140b}',
                Some("W"),
                warn,
                Some(icon_val),
            )
            .expect("Failed to write GPU PPT");
        }
    }
}

impl SyncBlockGenerator for Block {
    fn update(&mut self, block: &mut BlockOutputData) -> bool {
        let mut result = false;

        let usg = self
            .usage_buf
            .as_mut()
            .and_then(read_int_attr::<u8>)
            .unwrap_or(100);
        let gtt = self
            .gtt_bufs
            .as_mut()
            .and_then(Self::get_mem)
            .unwrap_or(100)
            .into();
        let temp = self.temp_buf.as_mut().and_then(Self::get_temp);
        let sclk = self.sclk_buf.as_mut().and_then(Self::get_clk);

        self.usg_filter.push(usg.into());
        self.gtt_filter.push(gtt);
        if let Some(c) = sclk {
            self.sclk_filter.push(c);
        }

        if let Some(t) = temp {
            let old2 = self.temp_filt.get_average2();
            self.temp_filt.push(t);
            let new2 = self.temp_filt.get_average2();
            if new2 != old2 {
                result = true;
            }
        }

        let new_ppt = self.ppt_buf.as_mut().and_then(Self::get_temp);
        if let Some(p) = new_ppt {
            let old2 = self.ppt_filt.get_average2();
            self.ppt_filt.push(p);
            let new2 = self.ppt_filt.get_average2();
            if new2 != old2 {
                result = true;
            }
        }

        if let Some(limits) = self.sclk_limits {
            result |= self.icon_meter.set_range_integer(
                self.sclk_filter.get_average2().into(),
                limits[0].into(),
                limits[1].into(),
            );
        }

        result |= self
            .usg_hex
            .set_range_integer(self.usg_filter.get_average2().into(), 0, 100);
        result |= self
            .gtt_hex
            .set_range_integer(self.gtt_filter.get_average2().into(), 0, 100);

        if result {
            self.write_output(block);
        }

        result
    }
}
