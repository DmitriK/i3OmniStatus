#![allow(clippy::non_ascii_literal)]
#![allow(clippy::cast_precision_loss)]
#![allow(clippy::similar_names)]

use crate::constants::IO_DEV;
use crate::pressure::{self, Pressure};
use crate::{
    blocks::{BlockOutputData, SyncBlockGenerator},
    nvme_temp::NvmeTemp,
};
use formatter::{format2digit, i3_write_to, round, DynamicWarn, PressureMeter, WarnLevel};
use iirfilter::SimpleFirFilter;
use std::convert::TryInto;
use std::fs::File;
use std::io::{BufRead, BufReader, Seek};
use std::path::PathBuf;
use std::str;
use std::time::Instant;

const MAX_READ: u32 = 1_500_000;
const MID_READ: u32 = 1_000_000;
const MAX_WRITE: u32 = 800_000;
const MID_WRITE: u32 = 400_000;
const MIN_READ: u32 = 500_000;
const MIN_WRITE: u32 = 80_000;

#[derive(Default, PartialEq)]
struct Values {
    pressure: Option<(u8, u8)>,
}

struct IoBlocks {
    read: u32,
    written: u32,
    timestamp: Instant,
}

pub struct Block {
    pressure: Option<Pressure<pressure::Io>>,
    pressure_meter: PressureMeter,
    // block_size: Option<u32>,
    stat_buffer: Option<BufReader<File>>,
    old_bytes: Option<IoBlocks>,
    read_filter: SimpleFirFilter<u32>,
    write_filter: SimpleFirFilter<u32>,
    old_speeds: Option<[u32; 2]>,
    nvme_temp: NvmeTemp,
}

impl Block {
    pub fn new() -> Self {
        let path: PathBuf = ["/sys", "block", IO_DEV, "stat"].iter().collect();
        let f = File::open(path).ok();
        let buf = f.map(|f| BufReader::with_capacity(512, f));

        Self {
            pressure: Pressure::<pressure::Io>::new(),
            pressure_meter: PressureMeter::default(),
            // block_size,
            stat_buffer: buf,
            old_bytes: None,
            read_filter: SimpleFirFilter::new(),
            write_filter: SimpleFirFilter::new(),
            old_speeds: None,
            nvme_temp: NvmeTemp::default(),
        }
    }

    fn get_rw_bytes(&mut self) -> Option<IoBlocks> {
        let buf = self.stat_buffer.as_mut()?;
        buf.rewind().ok()?;
        let b = buf.fill_buf().ok()?;
        let length = b.len();
        let line = unsafe { str::from_utf8_unchecked(b) };
        // Check that str ends in newline to ensure the read wasn't partial
        let mut tokens = line.strip_suffix('\n').map(str::split_whitespace);
        let num_read = tokens.as_mut().and_then(|t| t.nth(2)?.parse::<u64>().ok());
        let num_written = tokens.as_mut().and_then(|t| t.nth(3)?.parse::<u64>().ok());
        // Consume buffer after parse (slice no longer needed), but before checking
        // if parse is ok
        buf.consume(length);
        let num_read = num_read?.try_into().ok()?;
        let num_written = num_written?.try_into().ok()?;

        Some(IoBlocks {
            read: num_read,
            written: num_written,
            timestamp: Instant::now(),
        })
    }

    fn write_output(&self, block: &mut BlockOutputData) {
        block.full_text.clear();
        let out_str = &mut block.full_text;

        let r_log = self.read_filter.get_average2();
        let w_log = self.write_filter.get_average2();

        let r_log = if r_log > 0 {
            ((255 * r_log.ilog2()) / MAX_READ.ilog2()).clamp(0, 255) as u8
        } else {
            0
        };
        let w_log = if w_log > 0 {
            ((255 * w_log.ilog2()) / MAX_WRITE.ilog2()).clamp(0, 255) as u8
        } else {
            0
        };

        i3_write_to(
            out_str,
            &format2digit(self.read_filter.get_average2()),
            '\u{f0259}',
            None,
            WarnLevel::Dynamic(DynamicWarn {
                val: self
                    .read_filter
                    .get_average2()
                    .try_into()
                    .unwrap_or(i32::MAX),
                min: MIN_READ.try_into().unwrap_or(i32::MAX),
                mid: MID_READ.try_into().unwrap_or(i32::MAX),
                max: MAX_READ.try_into().unwrap_or(i32::MAX),
            }),
            Some(r_log),
        )
        .expect("Failed to write io read");
        out_str.push(' ');
        i3_write_to(
            out_str,
            &format2digit(self.write_filter.get_average2()),
            '\u{f024d}',
            None,
            WarnLevel::Dynamic(DynamicWarn {
                val: self
                    .write_filter
                    .get_average2()
                    .try_into()
                    .unwrap_or(i32::MAX),
                min: MIN_WRITE.try_into().unwrap_or(i32::MAX),
                mid: MID_WRITE.try_into().unwrap_or(i32::MAX),
                max: MAX_WRITE.try_into().unwrap_or(i32::MAX),
            }),
            Some(w_log),
        )
        .expect("Failed to write io write");

        out_str.push('\u{2009}');
        self.pressure_meter.write_to(out_str);
        self.nvme_temp.write_output(out_str);
    }
}

impl SyncBlockGenerator for Block {
    fn update(&mut self, block: &mut BlockOutputData) -> bool {
        let mut result = false;

        if let Some(p) = self.pressure.as_mut().and_then(Pressure::get) {
            self.pressure_meter.update(p.1.into(), p.0.into());
            result = true;
        }

        let new_bytes = self.get_rw_bytes();
        let speeds = if let (Some(new_data), Some(old_data)) =
            (new_bytes.as_ref(), self.old_bytes.as_ref())
        {
            if new_data.timestamp > old_data.timestamp {
                let time_delta: Option<u32> = (new_data.timestamp - old_data.timestamp)
                    .as_millis()
                    .try_into()
                    .ok();

                match time_delta {
                    Some(d) if d > 0 => {
                        // Block size is fixed at 512 bytes, so formula is:
                        // blocks * 512 B/block * 1/1024 KiB/B * 1000 ms/s
                        let read_speed = 500 * new_data.read.wrapping_sub(old_data.read) / d;
                        let write_speed = 500 * new_data.written.wrapping_sub(old_data.written) / d;

                        Some((read_speed, write_speed))
                    }
                    _ => None,
                }
            } else {
                // Times are same or backwards
                None
            }
        } else {
            None
        };
        self.old_bytes = new_bytes;

        if let Some(s) = speeds {
            let rs = s.0;
            let ws = s.1;

            self.read_filter.push(rs);
            self.write_filter.push(ws);

            let new_r = round::<10>(self.read_filter.get_average2());
            let new_w = round::<10>(self.write_filter.get_average2());
            let [old_r, old_w] = self.old_speeds.unwrap_or([0, 0]);

            result |= old_r.abs_diff(new_r) >= 1 || old_w.abs_diff(new_w) >= 1;
        }
        result |= self.nvme_temp.update();
        if result {
            self.write_output(block);
        }
        result
    }
}
