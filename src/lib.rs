use std::fs;
use std::fs::File;
use std::io::Read;
use std::io::{self, BufRead, BufReader, Cursor, Seek};
use std::str;
use std::time::SystemTime;

use murmur3::murmur3_x64_128;

#[derive(Debug)]
pub struct FileReader {
    reader: BufReader<File>,
    buffer: String,
}

impl FileReader {
    pub fn with_capacities(file: File, reader_size: usize, buffer_size: usize) -> Self {
        Self {
            reader: BufReader::with_capacity(reader_size, file),
            buffer: String::with_capacity(buffer_size),
        }
    }
    pub fn rewind(&mut self) -> Result<(), io::Error> {
        self.reader.rewind()?;
        self.buffer.clear();
        Ok(())
    }

    pub fn read_line(&mut self) -> Result<usize, io::Error> {
        self.reader.read_line(&mut self.buffer)
    }

    pub fn read_all(&mut self) -> Result<usize, io::Error> {
        self.reader.read_to_string(&mut self.buffer)
    }

    pub fn get_str(&self) -> &str {
        &self.buffer
    }
}

pub fn read_int_attr<T: str::FromStr>(buf: &mut BufReader<File>) -> Option<T> {
    buf.rewind().ok()?;
    let b = buf.fill_buf().ok()?;
    let length = b.len();
    let s = unsafe { str::from_utf8_unchecked(b) };
    // Check that str ends in newline to ensure the read wasn't partial
    let t = s.strip_suffix('\n').and_then(|s| s.parse::<T>().ok());
    // Consume buffer after parse (slice no longer needed), but before checking
    // if parse is ok
    buf.consume(length);
    t
}

pub fn get_albumart_hash(path: &str) -> Option<u128> {
    // Generate hash from filename, size, and modification date
    let meta = fs::metadata(path).ok()?;
    let name = path;
    let size = meta.len();
    let mtime = meta
        .modified()
        .ok()?
        .duration_since(SystemTime::UNIX_EPOCH)
        .ok()?
        .as_secs();

    let mut data: Vec<u8> = name.as_bytes().into();
    data.extend_from_slice(&size.to_ne_bytes());
    data.extend_from_slice(&mtime.to_ne_bytes());
    let data = &mut Cursor::new(data);
    murmur3_x64_128(data, 0).ok()
}
