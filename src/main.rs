#![allow(clippy::non_ascii_literal)]

mod blocks;
mod pressure;

#[cfg(HAS_BATTERY)]
mod batt;
mod clock;
mod constants;
mod cpu;
mod gpu;
mod io;
mod mem;
#[cfg(feature = "block_music")]
mod music;
mod net;
mod nvme_temp;
#[cfg(feature = "block_volume")]
mod volume;

use blocks::{BlockOutputData, SyncBlockGenerator};
use constants::UPDATE_PERIOD;
use palette::Srgb;
use parking_lot::Mutex;
use serde_derive::Deserialize;
use std::io::stdout;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread::{self, Thread};

#[derive(Deserialize)]
pub struct I3BarEvent {
    pub name: Option<String>,
    pub instance: Option<String>,
    pub x: u64,
    pub y: u64,
    pub button: u64,
}

struct BlockStrings {
    cpu: BlockOutputData,
    mem: BlockOutputData,
    io: BlockOutputData,
    gpu: BlockOutputData,
    net: BlockOutputData,
    #[cfg(HAS_BATTERY)]
    batt: BlockOutputData,
}

fn periodic_backend(block_strings: &Arc<Mutex<BlockStrings>>, main_thread: &Arc<Thread>) {
    // init
    let mut cpu_blk = cpu::Block::new();
    let mut mem_blk = mem::Block::new();
    let mut io_blk = io::Block::new();
    let mut gfx_blk = gpu::Block::new();
    let mut net_blk = net::Block::new();
    #[cfg(HAS_BATTERY)]
    let mut batt_blk = batt::Block::new();

    let mut iter: u8 = 0;

    // do updates and update shared state as needed
    loop {
        let mut gui_update_needed = false;

        {
            let mut block_data = block_strings.lock();

            gui_update_needed |= cpu_blk.update(&mut block_data.cpu);
            gui_update_needed |= mem_blk.update(&mut block_data.mem);
            gui_update_needed |= io_blk.update(&mut block_data.io);
            gui_update_needed |= gfx_blk.update(&mut block_data.gpu);
            gui_update_needed |= net_blk.update(&mut block_data.net);
            #[cfg(HAS_BATTERY)]
            {
                if iter % 4 == 0 {
                    gui_update_needed |= batt_blk.update(&mut block_data.batt);
                }
            }
        }
        iter = iter.wrapping_add(1);
        if gui_update_needed {
            main_thread.unpark();
        }
        thread::sleep(UPDATE_PERIOD);
    }
}

fn print_block(block: &BlockOutputData, is_last: bool) {
    if let Err(e) = serde_json::to_writer(stdout(), block) {
        println!("{{\"full_text\": \"Failed to generate JSON: {e}\"}}");
    }
    if !is_last {
        print!(",");
    }
}

#[allow(clippy::similar_names, clippy::non_ascii_literal)]
fn main() {
    let restart = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(signal_hook::consts::SIGHUP, Arc::clone(&restart))
        .expect("Failed to register signal handler");

    let curr_thread = Arc::new(thread::current());

    #[cfg(feature = "block_music")]
    let mut music_gen = music::Block::new(curr_thread.clone());
    #[cfg(feature = "block_volume")]
    let mut volume_gen = volume::Block::new(curr_thread.clone());
    let mut clock_gen = clock::Block::new(curr_thread.clone());

    #[cfg(feature = "block_music")]
    let mut music_block = BlockOutputData::new("Music", true);
    let cpu_block = BlockOutputData::new("CPU", true);
    let mem_block = BlockOutputData::new("Memory", true);
    let gpu_block = BlockOutputData::new("GPU", true);
    let io_block = BlockOutputData::new("I/O", true);
    let net_block = BlockOutputData::new("Network", true);
    #[cfg(HAS_BATTERY)]
    let batt_block = BlockOutputData::new("Battery", color_it.next().cloned(), true);
    #[cfg(feature = "block_volume")]
    let mut volume_block = BlockOutputData::new("Volume", true);
    let mut clock_block = BlockOutputData::new("Clock", true);

    let block_strings = Arc::new(Mutex::new(BlockStrings {
        cpu: cpu_block,
        mem: mem_block,
        io: io_block,
        gpu: gpu_block,
        net: net_block,
        #[cfg(HAS_BATTERY)]
        batt: batt_block,
    }));

    let block_strings_periodic = block_strings.clone();

    let _periodic_thread =
        thread::spawn(move || periodic_backend(&block_strings_periodic, &curr_thread));

    #[cfg(feature = "block_music")]
    let _music_input_thread = thread::Builder::new()
        .name("Input watcher".to_string())
        .spawn(move || {
            let mut input = String::new();
            loop {
                input.clear();
                std::io::stdin().read_line(&mut input).unwrap();

                let slice = input.trim_start_matches(|c| c != '{');
                let slice = slice.trim_end_matches(|c| c != '}');

                if !slice.is_empty() {
                    let e: I3BarEvent = serde_json::from_str(slice).unwrap();
                    if e.name == Some("Music".to_string()) {
                        music::handle_input(e.button);
                    }
                }
            }
        });

    if !std::env::args().any(|a| a == "--noinit") {
        println!("{{ \"version\": 1, \"click_events\": true }}");
        println!("[");
    }

    while !restart.load(Ordering::Relaxed) {
        thread::park();

        clock_gen.update(&mut clock_block);
        #[cfg(feature = "block_volume")]
        {
            volume_gen.update(&mut volume_block);
        }
        #[cfg(feature = "block_music")]
        {
            music_gen.update(&mut music_block);
        }

        print!("[");
        {
            let blks = block_strings.lock();
            #[cfg(feature = "block_music")]
            print_block(&music_block, false);
            print_block(&blks.cpu, false);
            print_block(&blks.mem, false);
            print_block(&blks.gpu, false);
            print_block(&blks.io, false);
            print_block(&blks.net, false);
            #[cfg(HAS_BATTERY)]
            print_block(&blks.batt, false);
            #[cfg(feature = "block_volume")]
            print_block(&volume_block, false);
            print_block(&clock_block, true);
        }
        println!("],");
    }

    volume_gen.close_child();

    let exec_err = exec::Command::new("i3omnistatus").arg("--noinit").exec();
    panic!("Failed to restart process: {exec_err}",);
}
