#![allow(clippy::non_ascii_literal)]
#![allow(clippy::cast_precision_loss)]

use crate::blocks::{BlockOutputData, SyncBlockGenerator};
use crate::pressure::{self, Pressure};
use formatter::{HexMeter, PressureMeter, StaticIconMeter};
use i3omnistatus::FileReader;
use iirfilter::SimpleFirFilter;
use std::convert::TryInto;
use std::{self, fmt::Write, fs::File};

struct MemValues {
    used: u32,  // stored as MiB
    total: u32, // stored as MiB
}

struct MemInfo {
    ram: MemValues,
    swap: MemValues,
    zswap: Option<MemValues>, // used is compressed, total is uncompressed
}

fn get_mem(buf: &mut FileReader) -> Option<MemInfo> {
    buf.rewind().ok()?;

    let mut avail = None;
    let mut total = None;
    let mut swap_avail = None;
    let mut swap_total = None;
    let mut zswap = None;
    let mut zswapped = None;

    buf.read_all().ok()?;
    let buf = buf.get_str();

    for line in buf.lines() {
        if total.is_none() && line.contains("MemTotal:") {
            let mut chunks = line.split_whitespace();
            total = chunks.nth(1).and_then(|c| c.parse::<u64>().ok());
        }
        if avail.is_none() && line.contains("MemAvailable:") {
            let mut chunks = line.split_whitespace();
            avail = chunks.nth(1).and_then(|c| c.parse::<u64>().ok());
        }
        if swap_total.is_none() && line.contains("SwapTotal:") {
            let mut chunks = line.split_whitespace();
            swap_total = chunks.nth(1).and_then(|c| c.parse::<u64>().ok());
        }
        if swap_avail.is_none() && line.contains("SwapFree:") {
            let mut chunks = line.split_whitespace();
            swap_avail = chunks.nth(1).and_then(|c| c.parse::<u64>().ok());
        }
        if zswap.is_none() && line.contains("Zswap:") {
            let mut chunks = line.split_whitespace();
            zswap = chunks.nth(1).and_then(|c| c.parse::<u64>().ok());
        }
        if zswapped.is_none() && line.contains("Zswapped:") {
            let mut chunks = line.split_whitespace();
            zswapped = chunks.nth(1).and_then(|c| c.parse::<u64>().ok());
        }
    }

    let to_mib = |v: u64| -> u32 { ((v + 512) / 1024).try_into().unwrap_or(u32::MAX) };

    if let (Some(av), Some(tot), Some(swp_av), Some(swp_tot)) =
        (avail, total, swap_avail, swap_total)
    {
        Some(MemInfo {
            ram: MemValues {
                used: to_mib(tot - av),
                total: to_mib(tot),
            },
            swap: MemValues {
                used: to_mib(swp_tot - swp_av),
                total: to_mib(swp_tot),
            },
            zswap: zswap.zip(zswapped).map(|(u, t)| MemValues {
                used: to_mib(u),
                total: to_mib(t),
            }),
        })
    } else {
        None
    }
}

pub struct Block {
    buffer: Option<FileReader>,
    pressure: Option<Pressure<pressure::Memory>>,
    pressure_meter: PressureMeter,
    icon_meter: StaticIconMeter<'\u{f2db}'>,
    mem_hex: HexMeter,
    swap_hex: HexMeter,
    zswap_hex: HexMeter,
    zcompr_hex: HexMeter,
    mem_filter: SimpleFirFilter<u16>,
    swap_filter: SimpleFirFilter<u16>,
    zswap_filter: SimpleFirFilter<u16>,
    zcompr_filter: SimpleFirFilter<u16>,
}

impl Block {
    const USE_WARN: u16 = 50;
    const USE_CRIT: u16 = 75;

    pub fn new() -> Self {
        let f = File::open("/proc/meminfo");
        let buffer = f.map(|f| FileReader::with_capacities(f, 2048, 2048)).ok();
        Self {
            buffer,
            pressure: Pressure::<pressure::Memory>::new(),
            pressure_meter: PressureMeter::default(),
            icon_meter: StaticIconMeter::default(),
            mem_hex: HexMeter::default(),
            swap_hex: HexMeter::default(),
            zswap_hex: HexMeter::default(),
            zcompr_hex: HexMeter::default(),
            mem_filter: SimpleFirFilter::default(),
            swap_filter: SimpleFirFilter::default(),
            zswap_filter: SimpleFirFilter::default(),
            zcompr_filter: SimpleFirFilter::default(),
        }
    }

    fn write_output(&self, block: &mut BlockOutputData) {
        block.full_text.clear();
        let out_str = &mut block.full_text;
        let _ = write!(
            out_str,
            "{} {}{}{}{} ",
            self.icon_meter, self.mem_hex, self.swap_hex, self.zswap_hex, self.zcompr_hex
        );

        self.pressure_meter.write_to(out_str);
    }
}

impl SyncBlockGenerator for Block {
    fn update(&mut self, block: &mut BlockOutputData) -> bool {
        let mut result = false;
        let mem = self.buffer.as_mut().and_then(get_mem);

        if let Some(MemInfo { ram, swap, zswap }) = mem {
            let m_usage = ((100 * ram.used + ram.total / 2) / ram.total)
                .try_into()
                .unwrap_or(100);
            let s_usage = if swap.total == 0 {
                0
            } else {
                ((100 * swap.used + swap.total / 2) / swap.total)
                    .try_into()
                    .unwrap_or(100)
            };
            self.mem_filter.push(m_usage);
            self.swap_filter.push(s_usage);

            if let Some(MemValues { used, total }) = zswap {
                let zswap_prop = if swap.total == 0 || swap.total < total {
                    0
                } else {
                    let wb = swap.used - total;
                    ((100 * wb + swap.total / 2) / swap.total)
                        .try_into()
                        .unwrap_or(100)
                };
                let zswap_cmpr = if total == 0 {
                    0
                } else {
                    ((100 * used + total / 2) / total).try_into().unwrap_or(100)
                };

                self.zswap_filter.push(zswap_prop);
                self.zcompr_filter.push(zswap_cmpr);
            }

            let total = ram.total + 2 * swap.total;
            // weighted average of both
            let overall_usage_filt = ((u32::from(self.mem_filter.get_average2()) * ram.total
                + u32::from(self.swap_filter.get_average2()) * swap.total * 2)
                / total)
                .try_into()
                .unwrap_or(u16::MAX);

            self.icon_meter
                .set_range_integer(overall_usage_filt.into(), 0, 100);

            result |= self
                .mem_hex
                .set_range_integer(self.mem_filter.get_average2().into(), 0, 100);
            result |=
                self.swap_hex
                    .set_range_integer(self.swap_filter.get_average2().into(), 0, 100);
            result |=
                self.zswap_hex
                    .set_range_integer(self.zswap_filter.get_average2().into(), 0, 100);
            result |=
                self.zcompr_hex
                    .set_range_integer(self.zcompr_filter.get_average2().into(), 0, 100);
        }

        if let Some(p) = self.pressure.as_mut().and_then(Pressure::get) {
            self.pressure_meter.update(p.1.into(), p.0.into());
            result = true;
        }

        if result {
            self.write_output(block);
        }

        result
    }
}
