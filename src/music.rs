#![allow(clippy::non_ascii_literal)]

use crate::blocks::{BlockOutputData, SyncBlockGenerator};
use i3omnistatus::get_albumart_hash;
use mpris::PlayerFinder;
use okolors::Okolors;
use palette::Mix;
use palette::{color_difference::HyAb, FromColor, Oklab, Srgb};
use parking_lot::Mutex;
use percent_encoding::percent_decode;
use rand::{rng, seq::IndexedRandom};
use serde::{Deserialize, Serialize};
use std::f32::consts::FRAC_1_SQRT_2;
use std::fs::{self, File};
use std::process::{Command, Stdio};
use std::sync::Arc;
use std::thread::{self, Thread};
use std::time;
use tempfile::NamedTempFile;

use formatter::{DECORATIVE_FONT, NERD_FONT};

const TARGET_PLAYER_IDENTITY: &str = "Audacious";

const IMAGE_RESIZE_LEN: u32 = 32;

#[allow(clippy::similar_names)]
#[allow(dead_code)]
fn debug_color<T: std::fmt::Display>(text: T, fg: Srgb<u8>, bg: Srgb<u8>) {
    let Srgb {
        red: fg_r,
        green: fg_g,
        blue: fg_b,
        standard: _,
    } = fg;
    let Srgb {
        red: bg_r,
        green: bg_g,
        blue: bg_b,
        standard: _,
    } = bg;
    eprint!("\x1b[38;2;{fg_r};{fg_g};{fg_b}m\x1b[48;2;{bg_r};{bg_g};{bg_b}m{text}\x1b[0m");
}

#[allow(dead_code)]
fn color_block(clr: Srgb<u8>) -> String {
    format!("\x1b[48;2;{};{};{}m \x1b[0m", clr.red, clr.green, clr.blue)
}

#[allow(dead_code)]
fn isqrt(x: u32) -> u16 {
    let mut res = 0;
    for bit in (1..16).rev() {
        let candidate = res | (1 << bit);
        // TODO: replace with widening_mul when stable
        let sqr = u32::from(candidate).pow(2);
        if sqr == x {
            return candidate;
        }
        if sqr < x {
            res = candidate;
        }
    }
    res
}

#[derive(Deserialize, Serialize)]
struct SavedPalette(Vec<PaletteColor>);

impl SavedPalette {
    fn dump(&self) {
        #[cfg(feature = "debug_music_colors")]
        {
            for c in &self.0 {
                eprint!("{}", color_block(Srgb::from_color(c.color).into_format()));
            }
            eprintln!();
        }
    }
}

#[derive(Deserialize, Serialize)]
struct PaletteColor {
    color: Oklab,
    bg_bias: u8,
}

impl PaletteColor {
    const MIN_L_DIST: f32 = 0.5;

    #[allow(dead_code)]
    fn get_srgb(&self) -> Srgb<u8> {
        Srgb::from_color(self.color).into_format()
    }

    fn is_contrasty(&self, other: &Self) -> bool {
        (self.color.l - other.color.l).abs() >= Self::MIN_L_DIST
    }
}

fn oklab_distance_extra_chroma(a: &Oklab, b: &Oklab) -> f32 {
    ((a.l - b.l).powi(2) + 2.0 * (a.a - b.a).powi(2) + 2.0 * (a.b - b.b).powi(2)).sqrt()
}

fn get_colors(results: &[&PaletteColor]) -> [Srgb<u8>; 3] {
    let mut triplet = [&results[0], &results[1], &results[2]];

    if results.len() > 3 {
        let mut best_score = 0;

        // always use value. Loop through remaining indices looking for best triplet
        for index2 in 1..results.len() {
            for index3 in (index2 + 1)..results.len() {
                // dbg!(index1, index2, index3);
                // if ![index1, index2, index3].contains(&0) {
                //     continue;
                // }
                let x = &results[0].color;
                let y = &results[index2].color;
                let z = &results[index3].color;
                let xy = oklab_distance_extra_chroma(x, y);
                let yz = oklab_distance_extra_chroma(y, z);
                let zx = oklab_distance_extra_chroma(z, x);

                let length = xy + yz + zx;
                #[allow(clippy::cast_possible_truncation)]
                let score = (64.0 * length).round() as i32;
                #[cfg(feature = "debug_music_colors")]
                eprint!(
                    "Considering {x_blk}{y_blk}{z_blk}: {length}={score}",
                    x_blk = color_block(Srgb::from_color(*x).into_format()),
                    y_blk = color_block(Srgb::from_color(*y).into_format()),
                    z_blk = color_block(Srgb::from_color(*z).into_format()),
                );

                if score > best_score {
                    best_score = score;
                    triplet = [&results[0], &results[index2], &results[index3]];
                    #[cfg(feature = "debug_music_colors")]
                    eprint!("*");
                }

                #[cfg(feature = "debug_music_colors")]
                eprintln!();
            }
        }

        #[cfg(feature = "debug_music_colors")]
        {
            eprintln!(
                "Best triplet:\t{}{}{}({})",
                color_block(Srgb::from_color(triplet[0].color).into_format()),
                color_block(Srgb::from_color(triplet[1].color).into_format()),
                color_block(Srgb::from_color(triplet[2].color).into_format()),
                best_score,
            );
        }
    }

    // Determine most common color to select background, but use biased bg_count
    if triplet[1].bg_bias > triplet[0].bg_bias {
        triplet.swap(0, 1);
    }
    if triplet[2].bg_bias > triplet[0].bg_bias {
        triplet.swap(0, 2);
    }
    let bg = triplet[0];

    // With background selected, pick highest contrast foreground
    let contrast_score = |x: &PaletteColor| (x.color.l - bg.color.l).abs();
    if contrast_score(triplet[2]) > contrast_score(triplet[1]) {
        triplet.swap(1, 2);
    }
    let fg = triplet[1];

    // Remaining color is accent
    let ac = triplet[2];

    let [fg2, bg2] = stretch_contrast(fg, bg);

    #[cfg(feature = "debug_music_colors")]
    {
        debug_color(
            "Triplet",
            Srgb::from_color(fg.color).into_format(),
            Srgb::from_color(bg.color).into_format(),
        );
        eprint!(" > ");
        debug_color("Final", fg2, bg2);
        eprintln!();
        debug_color(
            "       ",
            Srgb::from_color(fg.color).into_format(),
            Srgb::from_color(ac.color).into_format(),
        );
        eprint!(" > ");
        debug_color("     ", fg2, Srgb::from_color(ac.color).into_format());
        eprintln!();
    }

    [bg2, fg2, ac.get_srgb()]
}

fn stretch_contrast(xp: &PaletteColor, yp: &PaletteColor) -> [Srgb<u8>; 2] {
    // println!("Stretching colors between {:?} and {:?}", xp, yp);

    if xp.is_contrasty(yp) {
        return [xp.get_srgb(), yp.get_srgb()];
    }

    #[cfg(feature = "debug_music_colors")]
    eprintln!("Need to stretch {} vs {}", xp.color.l, yp.color.l);

    let (smaller, larger) = if xp.color.l < yp.color.l {
        (&xp.color, &yp.color)
    } else {
        (&yp.color, &xp.color)
    };

    // Calculate equal scaling percentage to get contrast
    let scale_pct = (smaller.l - larger.l + PaletteColor::MIN_L_DIST) / (larger.l + smaller.l);
    #[cfg(feature = "debug_music_colors")]
    eprintln!("uniform stretch scale factor: {scale_pct}");

    let mut new_larger = *larger * (1.0 + scale_pct);
    let mut new_smaller = *smaller * (1.0 - scale_pct);

    if new_larger.l > 1.0 {
        // If we need to clamp on the high side, re-calculate
        #[cfg(feature = "debug_music_colors")]
        eprintln!("Clamping on high side");
        new_larger = new_larger * (1.0 / new_larger.l);
        let scale = (1.0 - PaletteColor::MIN_L_DIST) / smaller.l;
        new_smaller = *smaller * scale;
    } else if new_smaller.l < 0.0 {
        #[cfg(feature = "debug_music_colors")]
        eprintln!("Clamping on low side");
        // If we need to clamp on the low side, re-calculate
        new_smaller = Oklab::new(0.0, 0.0, 0.0);
        let scale = PaletteColor::MIN_L_DIST / larger.l;
        new_larger = *larger * scale;
    }

    let (new_x, new_y) = if xp.color.l < yp.color.l {
        (new_smaller, new_larger)
    } else {
        (new_larger, new_smaller)
    };

    #[cfg(feature = "debug_music_colors")]
    eprintln!("Post-stretch {} vs {}", new_x.l, new_y.l);

    let rgb_x = Srgb::from_color(new_x).into_format();
    let rgb_y = Srgb::from_color(new_y).into_format();
    [rgb_x, rgb_y]
}

fn read_palette_from_file(f: File) -> Option<SavedPalette> {
    bincode::deserialize_from(f).ok()
}

fn get_error(img: image::buffer::Pixels<image::Rgb<u8>>, pal: &[Oklab]) -> f32 {
    let mut max_error = 0.0;
    for pixel in img {
        let mut min_error = f32::MAX;
        for color in pal {
            let pixel = Srgb::new(pixel[0], pixel[1], pixel[2]);
            let pixel = Oklab::from_color(pixel.into_format());
            let delta = pixel.hybrid_distance(*color);
            if delta < min_error {
                min_error = delta;
            }
            if delta == 0.0 {
                break;
            }
        }
        if min_error > max_error {
            max_error = min_error;
        }
    }
    max_error
}

fn get_palette(path: &str) -> Option<SavedPalette> {
    let hash = get_albumart_hash(path)?;
    let filename = format!("{hash:032x}.bin");
    let mut cache_path = dirs::cache_dir()?;
    cache_path.push("i3omnistatus");
    cache_path.push("music");
    cache_path.push("thumbnails");
    let mut thumb_path = cache_path.clone();
    thumb_path.push(filename);
    if let Ok(f) = File::open(&thumb_path) {
        if let Some(p) = read_palette_from_file(f) {
            #[cfg(feature = "debug_music_colors")]
            eprintln!("Loaded palette {hash:032x}.bin");
            p.dump();
            return Some(p);
        }
        let _ = fs::remove_file(path);
    }
    eprintln!("Need to generate new palette");
    // Generate new palette
    let tfile = NamedTempFile::with_suffix(".png").ok()?;

    Command::new("magick")
        .args([
            path,
            "-colorspace",
            "OkLab",
            "-adaptive-resize",
            &format!("{IMAGE_RESIZE_LEN}x{IMAGE_RESIZE_LEN}!"),
            "-colorspace",
            "sRGB",
            "-quality",
            "1",
        ])
        .arg(tfile.path())
        .stdout(Stdio::null())
        .status()
        .ok()?;

    let img = image::open(tfile.path()).ok()?.into_rgb8();
    // dbg!(&img);
    let mut oklr = Okolors::try_from(&img).ok()?;
    oklr.sampling_factor(1.0);
    oklr.sort_by_frequency(true);
    oklr.lightness_weight(FRAC_1_SQRT_2);

    let mut pal = Vec::new();

    for palette_size in (3_u16..=256) {
        oklr.palette_size(okolors::PaletteSize::from_clamped(palette_size));
        pal.clear();
        pal.append(&mut oklr.oklab_palette());
        let err = get_error(img.pixels(), &pal);
        #[cfg(feature = "debug_music_colors")]
        {
            for c in &pal {
                eprint!("{}", color_block(Srgb::from_color(*c).into_format()));
            }
            eprint!(" -> {err}");
            eprintln!();
        }
        if err < 0.2 {
            break;
        }
    }
    match pal.len() {
        0 => return None,
        1 => {
            pal.push(pal[0]);
            pal.push(pal[0]);
        }
        2 => {
            let avg = pal[0].mix(pal[1], 0.5);
            pal.push(avg);
        }
        _ => {}
    }
    #[allow(clippy::cast_possible_truncation)]
    let result: Vec<PaletteColor> = pal
        .iter()
        .enumerate()
        .map(|(i, c)| PaletteColor {
            color: *c,
            bg_bias: (255 * (i + i / pal.len()) / pal.len()).clamp(0, 255) as u8,
        })
        .collect();
    // Attempt to save
    let _ = fs::create_dir_all(cache_path);
    if let Ok(f) = File::create(thumb_path) {
        let _ = bincode::serialize_into(f, &result);
    }
    Some(SavedPalette(result))
}

#[allow(clippy::similar_names)]
fn get_colors_from_img(path: &str) -> Option<(Srgb<u8>, Srgb<u8>, Srgb<u8>)> {
    let p = get_palette(path)?;

    let sampled: Vec<&PaletteColor> = p.0.choose_multiple(&mut rng(), 8).collect();

    #[cfg(feature = "debug_music_colors")]
    {
        // for row in 0..thumb.height() {
        //     for col in 0..thumb.width() {
        //         let px = thumb.get_pixel(col, row);
        //         let clr = Srgb::new(px[0], px[1], px[2]);
        //         eprint!("{}", color_block(clr));
        //     }
        //     eprintln!();
        // }
        for sample in &sampled {
            let blk = color_block(Srgb::from_color(sample.color).into_format());
            eprint!("{blk}{blk}{blk}{blk}",);
        }
        eprintln!();
        for sample in &sampled {
            eprint!("{:4}", sample.bg_bias);
        }
        eprintln!();
    }
    let [bg_clr, fg_clr, acc_clr] = get_colors(&sampled);
    Some((fg_clr, bg_clr, acc_clr))
}

struct Metadata {
    status: Option<mpris::PlaybackStatus>,
    title: Option<String>,
    artist: Option<String>,
    album: Option<String>,
    colors: Option<(Srgb<u8>, Srgb<u8>, Srgb<u8>)>,
}

struct FormatSet {
    long: String,
    short: String,
}

impl Metadata {
    fn new(player: &mpris::Player) -> Self {
        let status = player.get_playback_status().ok();
        let Ok(meta) = player.get_metadata() else {
            return Self {
                status,
                title: None,
                artist: None,
                album: None,
                colors: None,
            };
        };
        let artist = meta.artists().and_then(|x| {
            if let Some(&s) = x.first() {
                Some(htmlescape::encode_minimal(s))
            } else {
                None
            }
        });
        let album = meta.album_name().map(htmlescape::encode_minimal);
        let title = meta.title().map(htmlescape::encode_minimal);
        let art_path = meta.art_url().map(|url| {
            percent_decode(url.trim_start_matches("file://").as_bytes())
                .decode_utf8_lossy()
                .into_owned()
        });

        let colors = art_path.as_ref().and_then(|p| get_colors_from_img(p));

        Self {
            status,
            title,
            artist,
            album,
            colors,
        }
    }

    fn update_status(&mut self, player: &mpris::Player) {
        self.status = player.get_playback_status().ok();
    }

    fn handle_event(&mut self, event: mpris::Event) -> bool {
        match event {
            mpris::Event::Paused => {
                self.status = Some(mpris::PlaybackStatus::Paused);
                true
            }
            mpris::Event::Playing => {
                self.status = Some(mpris::PlaybackStatus::Playing);
                true
            }
            mpris::Event::Stopped => {
                self.status = Some(mpris::PlaybackStatus::Stopped);
                true
            }
            mpris::Event::TrackChanged(m) => {
                self.artist = m.artists().and_then(|x| {
                    if let Some(&s) = x.first() {
                        Some(htmlescape::encode_minimal(s))
                    } else {
                        None
                    }
                });
                self.album = m.album_name().map(htmlescape::encode_minimal);
                self.title = m.title().map(htmlescape::encode_minimal);
                let art_path = m.art_url().map(|url| {
                    percent_decode(url.trim_start_matches("file://").as_bytes())
                        .decode_utf8_lossy()
                        .into_owned()
                });
                self.colors = art_path.as_ref().and_then(|p| get_colors_from_img(p));
                true
            }
            _ => false,
        }
    }

    fn format(&self) -> FormatSet {
        let status_icon = match self.status {
            Some(mpris::PlaybackStatus::Playing) => '\u{f144}',
            Some(mpris::PlaybackStatus::Paused) => '\u{f28b}',
            Some(mpris::PlaybackStatus::Stopped) => '\u{f28d}',
            _ => '\u{fc59}',
        };

        let title: &str = match self.title {
            Some(ref t) => t,
            _ => "?",
        };
        let artist: &str = match self.artist {
            Some(ref t) => t,
            _ => "Unknown artist",
        };
        let album: &str = match self.album {
            Some(ref t) => t,
            _ => "Unknown album",
        };

        FormatSet {
            long: format!("<span font_family=\"{NERD_FONT}\">{status_icon}</span><span font_family=\"{DECORATIVE_FONT}\"> {title}\u{2009}\u{2014}\u{2009}{artist} [{album}] </span>"),
            short: format!("<span font_family=\"{NERD_FONT}\">{status_icon}</span> <span font_family=\"{DECORATIVE_FONT}\">{title}</span>"),
        }
    }
}

pub fn handle_input(btn: u64) {
    if let Some(players) = mpris::PlayerFinder::new()
        .ok()
        .and_then(|pf| pf.find_all().ok())
    {
        if let Some(player) = players
            .iter()
            .find(|p| p.identity() == TARGET_PLAYER_IDENTITY)
        {
            match btn {
                1 => {
                    // Left click
                    let _ = player.next();
                }
                2 => {
                    // Middle click
                    let _ = player.play_pause();
                }
                3 => {
                    // Right click
                    let _ = player.previous();
                }
                _ => {}
            };
        }
    }
}

struct MusicCache {
    full_text: String,
    short_text: Option<String>,
    fg_color: Option<Srgb<u8>>,
    bg_color: Option<Srgb<u8>>,
    border_color: Option<Srgb<u8>>,
    updated: bool,
}

impl MusicCache {
    fn new() -> Self {
        Self {
            full_text: String::new(),
            short_text: None,
            fg_color: None,
            bg_color: None,
            border_color: None,
            updated: true,
        }
    }
    fn set_error(&mut self, msg: &str) {
        self.full_text = msg.into();
        self.fg_color = Some(Srgb::new(255, 0, 0));
        self.bg_color = None;
        self.border_color = None;
        self.updated = true;
    }
}

pub struct Block {
    metadata: Arc<Mutex<MusicCache>>,
}

impl Block {
    pub fn new(channel: Arc<Thread>) -> Self {
        let result = Self {
            metadata: Arc::new(Mutex::new(MusicCache::new())),
        };
        let md_c = result.metadata.clone();
        thread::Builder::new()
            .name("Music".into())
            .spawn(move || music_thread(&md_c, &channel))
            .unwrap();
        result
    }

    fn write_output(block: &mut BlockOutputData, metadata: &MusicCache) {
        block.full_text.clone_from(&metadata.full_text.clone());
        block.short_text.clone_from(&metadata.short_text.clone());
        block.color = metadata.fg_color;
        block.background = metadata.bg_color;
        if let Some(bc) = metadata.border_color {
            block.border = bc;
        }
    }
}

impl SyncBlockGenerator for Block {
    fn update(&mut self, block: &mut BlockOutputData) -> bool {
        let mut md = self.metadata.lock();
        if md.updated {
            md.updated = false;
            Self::write_output(block, &md);
            return true;
        }
        false
    }
}

fn update_block(metadata: &Metadata, music_cache: &Arc<Mutex<MusicCache>>, channel: &Arc<Thread>) {
    let mut o = music_cache.lock();
    let text = metadata.format();
    let (fg, bg, ac) = match metadata.colors {
        Some((fg, bg, ac)) => (Some(fg), Some(bg), Some(ac)),
        _ => (None, None, None),
    };
    o.full_text = text.long;
    o.short_text = Some(text.short);
    o.fg_color = fg;
    o.bg_color = bg;
    o.border_color = ac;
    o.updated = true;
    channel.unpark();
}

fn music_thread(music_cache: &Arc<Mutex<MusicCache>>, channel: &Arc<Thread>) {
    loop {
        let finder = match PlayerFinder::new() {
            Ok(f) => f,
            Err(e) => {
                music_cache
                    .lock()
                    .set_error(&format!("Failed to create playerfinder: {e}"));
                channel.unpark();
                thread::sleep(time::Duration::from_secs(30));
                continue;
            }
        };

        loop {
            let Ok(player) = finder.find_active() else {
                {
                    let mut o = music_cache.lock();
                    o.full_text = "\u{266c} No player".into();
                    o.short_text = Some("\u{266c}".into());
                    o.fg_color = None;
                    o.bg_color = None;
                    o.border_color = None;
                    o.updated = true;
                }
                channel.unpark();
                thread::sleep(time::Duration::from_secs(10));
                continue;
            };

            let events = match player.events() {
                Ok(e) => e,
                Err(e) => {
                    music_cache
                        .lock()
                        .set_error(&format!("Failed to create playerevents: {e}"));
                    channel.unpark();
                    thread::sleep(time::Duration::from_secs(30));
                    continue;
                }
            };

            let mut md = Metadata::new(&player);

            update_block(&md, music_cache, channel);

            for event in events {
                match event {
                    Ok(e) => {
                        if matches!(e, mpris::Event::PlayerShutDown) {
                            break;
                        }
                        if md.handle_event(e) {
                            update_block(&md, music_cache, channel);
                        }
                    }
                    Err(e) => {
                        music_cache.lock().set_error(&format!("Event error: {e}"));
                        channel.unpark();
                        thread::sleep(time::Duration::from_secs(30));
                        continue;
                    }
                }
            }
        }
    }
}
