#[derive(Clone, Copy, Debug, Default)]
pub struct ICtCp {
    pub i: f32,
    pub ct: f32,
    pub cp: f32,
}

fn srgb2linear(n: f32) -> f32 {
    if n > 0.04045 {
        ((n + 0.055) / 1.055).powf(2.4)
    } else {
        n / 12.92
    }
}

fn linear2srgb(n: f32) -> f32 {
    if n <= 0.0031308 {
        12.92 * n
    } else {
        1.055 * n.powf(1. / 2.4) - 0.055
    }
}

// fn hlg(n: f32) -> f32 {
//     if n <= 1. {
//         0.5 * n.sqrt()
//     } else {
//         0.17883277 * (n - 0.28466892).ln() + 0.55991073
//     }
// }

pub fn pq_eotf_1(y: f32) -> f32 {
    // Do everything as f64, as there is too much rounding error otherwise
    const M_1: f64 = 0.1593017578125;
    const M_2: f64 = 78.84375;
    const C_1: f64 = 0.8359375;
    const C_2: f64 = 18.8515625;
    const C_3: f64 = 18.6875;

    let y: f64 = y.into();
    let y = y.powf(M_1);

    ((C_1 + C_2 * y) / (1. + C_3 * y)).powf(M_2) as f32
}

pub fn pq_eotf(x: f32) -> f32 {
    const M_1: f64 = 0.1593017578125;
    const M_2: f64 = 78.84375;
    const C_1: f64 = 0.8359375;
    const C_2: f64 = 18.8515625;
    const C_3: f64 = 18.6875;

    let x: f64 = x.into();
    let x_m = x.powf(1. / M_2);

    let res = (-(x_m - C_1) / (C_3 * x_m - C_2)).powf(1. / M_1);
    if res.is_nan() {
        0.
    } else {
        res as f32
    }
}

impl ICtCp {
    pub fn from_srgb(rgb: [u8; 3]) -> Self {
        let lin = [
            srgb2linear(f32::from(rgb[0]) / 255.),
            srgb2linear(f32::from(rgb[1]) / 255.),
            srgb2linear(f32::from(rgb[2]) / 255.),
        ];

        let lms = [
            0.29579 * lin[0] + 0.62313 * lin[1] + 0.08110 * lin[2],
            0.15620 * lin[0] + 0.72736 * lin[1] + 0.11643 * lin[2],
            0.03511 * lin[0] + 0.15658 * lin[1] + 0.80826 * lin[2],
        ];

        let lms_p = [pq_eotf_1(lms[0]), pq_eotf_1(lms[1]), pq_eotf_1(lms[2])];

        ICtCp {
            i: 2048. / 4096. * lms_p[0] + 2048. / 4096. * lms_p[1],
            ct: 6610. / 4096. * lms_p[0] - 13613. / 4096. * lms_p[1] + 7003. / 4096. * lms_p[2],
            cp: 17933. / 4096. * lms_p[0] - 17390. / 4096. * lms_p[1] - 543. / 4096. * lms_p[2],
        }
    }

    pub fn to_srgb(&self) -> [u8; 3] {
        let lms_p = [
            1.00000 * self.i + 0.00861 * self.ct + 0.11103 * self.cp,
            1.00000 * self.i - 0.00861 * self.ct - 0.11103 * self.cp,
            1.00000 * self.i + 0.56003 * self.ct - 0.32063 * self.cp,
        ];

        let lms = [pq_eotf(lms_p[0]), pq_eotf(lms_p[1]), pq_eotf(lms_p[2])];

        let lin = [
            6.17331 * lms[0] - 5.32067 * lms[1] + 0.14724 * lms[2],
            -1.32354 * lms[0] + 2.55980 * lms[1] - 0.23621 * lms[2],
            -0.01145 * lms[0] - 0.26506 * lms[1] + 1.27660 * lms[2],
        ];

        let srgb = [
            (255. * linear2srgb(lin[0])).round(),
            (255. * linear2srgb(lin[1])).round(),
            (255. * linear2srgb(lin[2])).round(),
        ];

        [
            if srgb[0] < 0. {
                0
            } else if srgb[0] > 255. {
                255
            } else {
                srgb[0] as u8
            },
            if srgb[1] < 0. {
                0
            } else if srgb[1] > 255. {
                255
            } else {
                srgb[1] as u8
            },
            if srgb[2] < 0. {
                0
            } else if srgb[2] > 255. {
                255
            } else {
                srgb[2] as u8
            },
        ]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[ignore]
    fn conversion() {
        let close = |a: [u8; 3], b: [u8; 3]| -> bool {
            (a[0] as i32 - b[0] as i32).abs() <= 1
                && (a[1] as i32 - b[1] as i32).abs() <= 1
                && (a[2] as i32 - b[2] as i32).abs() <= 1
        };

        for r in 0..=255 {
            for g in 0..=255 {
                for b in 0..=255 {
                    let rgb = [r, g, b];
                    let itp = ICtCp::from_srgb(rgb);
                    assert!(close(rgb, itp.to_srgb()));
                }
            }
        }
    }
}
