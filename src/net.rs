#![allow(clippy::non_ascii_literal)]
#![allow(clippy::similar_names)]

use crate::blocks::{BlockOutputData, SyncBlockGenerator};
use formatter::{format2digit, i3_write_to, round, DynamicWarn, WarnLevel};
use i3omnistatus::FileReader;
use iirfilter::SimpleFirFilter;
use std::convert::TryInto;
use std::fs::File;
use std::time::Instant;

const BW_RX: u32 = 320_000;
const BW_TX: u32 = 320_000;
const INTERFACES: [&str; 1] = ["enp1s0"];

const BW_RX_MIN: u32 = 10_000; // 10Mbps
const BW_TX_MIN: u32 = 10_000; // 10Mbps
const BW_RX_MID: u32 = 100_000; // 100Mpbs
const BW_TX_MID: u32 = 100_000; // 100Mpbs

struct Netbytes {
    rx: u64,
    tx: u64,
    ts: Instant,
}

fn get_dev_bytes(buf: &mut FileReader) -> Option<Netbytes> {
    buf.rewind().ok()?;
    buf.read_all().ok()?;
    let ts = Instant::now();

    let itr = buf.get_str().lines().filter(|line| {
        for ifc in &INTERFACES {
            if line.trim().starts_with(ifc) {
                return true;
            }
        }
        false
    });

    let (mut rx, mut tx) = (0, 0);

    for line in itr {
        let mut chunks = line.split_whitespace();

        rx += chunks.nth(1).and_then(|x| x.parse::<u64>().ok())?;
        tx += chunks.nth(7).and_then(|x| x.parse::<u64>().ok())?;
    }
    // Store in kbits
    rx *= 8;
    tx *= 8;
    rx /= 1000;
    tx /= 1000;
    Some(Netbytes { rx, tx, ts })
}

#[derive(Default)]
struct Values<T> {
    rx: T,
    tx: T,
}

pub struct Block {
    cache: Values<SimpleFirFilter<u32>>, // store as KiB/s
    netdev_buf: Option<FileReader>,
    old_bytes: Option<Netbytes>,
    old_speeds: Option<[u32; 2]>,
}

impl Block {
    pub fn new() -> Self {
        let f = File::open("/proc/net/dev").ok();
        let buf = f.map(|f| FileReader::with_capacities(f, 2048, 512));
        Self {
            cache: Values::default(),
            netdev_buf: buf,
            old_bytes: None,
            old_speeds: None,
        }
    }

    pub fn write_output(&self, block: &mut BlockOutputData) {
        let Values { rx, tx } = &self.cache;
        block.full_text.clear();
        let out_str = &mut block.full_text;

        let rx_log = rx.get_average2();
        let tx_log = tx.get_average2();

        let rx_log = if rx_log > 0 {
            ((255 * rx_log.ilog2()) / BW_RX.ilog2()).clamp(0, 255) as u8
        } else {
            0
        };
        let tx_log = if tx_log > 0 {
            ((255 * tx_log.ilog2()) / BW_TX.ilog2()).clamp(0, 255) as u8
        } else {
            0
        };

        i3_write_to(
            out_str,
            &format2digit(rx.get_average2()),
            '\u{f0162}',
            None,
            WarnLevel::Dynamic(DynamicWarn {
                val: rx.get_average2().try_into().unwrap_or(i32::MAX),
                min: BW_RX_MIN.try_into().unwrap_or(i32::MAX),
                mid: BW_RX_MID.try_into().unwrap_or(i32::MAX),
                max: BW_RX.try_into().unwrap_or(i32::MAX),
            }),
            Some(rx_log),
        )
        .expect("Failed to write net rx");
        out_str.push(' ');

        i3_write_to(
            out_str,
            &format2digit(tx.get_average2()),
            '\u{f0167}',
            None,
            WarnLevel::Dynamic(DynamicWarn {
                val: tx.get_average2().try_into().unwrap_or(i32::MAX),
                min: BW_TX_MIN.try_into().unwrap_or(i32::MAX),
                mid: BW_TX_MID.try_into().unwrap_or(i32::MAX),
                max: BW_TX.try_into().unwrap_or(i32::MAX),
            }),
            Some(tx_log),
        )
        .expect("Failed to write net tx");
    }
}

impl SyncBlockGenerator for Block {
    fn update(&mut self, block: &mut BlockOutputData) -> bool {
        let new_bytes = self.netdev_buf.as_mut().and_then(get_dev_bytes);

        let speeds: Values<u32> = match (new_bytes.as_ref(), self.old_bytes.as_ref()) {
            (Some(nb), Some(ob)) => {
                // The /proc/net/dev values can overflow, so need to ensure things are monotonic
                #[allow(clippy::cast_possible_truncation)]
                let dur = (nb.ts - ob.ts).as_secs() as u32;
                if dur >= 1 && nb.rx >= ob.rx && nb.tx >= ob.tx {
                    #[allow(clippy::cast_possible_truncation)]
                    let delta_rx = (nb.rx - ob.rx) as u32;
                    #[allow(clippy::cast_possible_truncation)]
                    let delta_tx = (nb.tx - ob.tx) as u32;
                    Values {
                        rx: delta_rx / dur,
                        tx: delta_tx / dur,
                    }
                } else {
                    Values::default()
                }
            }
            _ => Values::default(),
        };
        self.old_bytes = new_bytes;

        self.cache.rx.push(speeds.rx);
        self.cache.tx.push(speeds.tx);
        let new_rx = round::<10>(self.cache.rx.get_average2());
        let new_tx = round::<10>(self.cache.tx.get_average2());

        let [old_rx, old_tx] = self.old_speeds.unwrap_or([0, 0]);

        if old_rx.abs_diff(new_rx) >= 1 || old_tx.abs_diff(new_tx) >= 1 {
            self.old_speeds = Some([new_rx, new_tx]);
            self.write_output(block);
            true
        } else {
            false
        }
    }
}
