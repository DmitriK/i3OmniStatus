use std::{
    fs::File,
    io::{BufReader, Read},
};

use formatter::{i3_write_to, DynamicWarn, WarnLevel};
use i3omnistatus::read_int_attr;

use glob::glob;

#[derive(Default)]
pub struct NvmeTemp {
    buf: Option<BufReader<File>>,
    temp_warn: Option<i32>,
    temp_crit: Option<i32>,
    temp: Option<i32>,
}

impl NvmeTemp {
    fn init(&mut self) {
        if !cfg!(HAS_NVME_TEMP) {
            return;
        }
        self.buf = glob("/sys/class/nvme/nvme*/hwmon*/temp*_input")
            .ok()
            .and_then(|mut p| p.next())
            .and_then(|p| p.ok())
            .and_then(|p| File::open(p).ok())
            .map(|f| BufReader::with_capacity(8, f));
        let mut buf = String::new();
        self.temp_warn = glob("/sys/class/nvme/nvme*/hwmon*/temp*_max")
            .ok()
            .and_then(|mut p| p.next())
            .and_then(|p| p.ok())
            .and_then(|p| File::open(p).ok())
            .and_then(|mut f| f.read_to_string(&mut buf).ok())
            .and_then(|_| buf.trim().parse().ok());
        buf.clear();
        self.temp_crit = glob("/sys/class/nvme/nvme*/hwmon*/temp*_crit")
            .ok()
            .and_then(|mut p| p.next())
            .and_then(|p| p.ok())
            .and_then(|p| File::open(p).ok())
            .and_then(|mut f| f.read_to_string(&mut buf).ok())
            .and_then(|_| buf.trim().parse().ok());
    }
    pub fn update(&mut self) -> bool {
        if !cfg!(HAS_NVME_TEMP) {
            return false;
        }
        if self.buf.is_none() {
            self.init();
        }
        let new_temp = self.buf.as_mut().and_then(read_int_attr::<i32>);
        let changed = new_temp != self.temp;
        self.temp = new_temp;
        changed
    }

    pub fn write_output(&self, output: &mut String) {
        if !cfg!(HAS_NVME_TEMP) {
            return;
        }
        if let Some(t) = self.temp {
            let warn = match (self.temp_warn, self.temp_crit) {
                (Some(warn), Some(crit)) => WarnLevel::Dynamic(DynamicWarn {
                    val: t,
                    min: (warn - (crit - warn)),
                    mid: (warn),
                    max: (crit),
                }),
                (_, Some(crit)) if t >= crit => WarnLevel::Crit,
                (Some(warn), _) if t >= warn => WarnLevel::Warn,
                _ => WarnLevel::None,
            };

            let icon = match (self.temp_warn, self.temp_crit) {
                (Some(warn), _) if t >= warn => '\u{F2C7}',
                (Some(warn), Some(crit)) if t >= warn - (crit - warn) => '\u{F2C8}',
                (Some(warn), Some(crit)) if t >= warn - 2 * (crit - warn) => '\u{F2C9}',
                (Some(warn), Some(crit)) if t >= warn - 3 * (crit - warn) => '\u{F2CA}',
                _ => '\u{F2CB}',
            };
            let icon_val = match (self.temp_warn, self.temp_crit) {
                (Some(warn), Some(crit)) => {
                    let t_lo = warn - (crit - warn);
                    let t_hi = warn;
                    #[allow(clippy::cast_sign_loss)]
                    Some(((255 * (t - t_lo)) / (t_hi - t_lo)).clamp(0, 255) as u8)
                }
                _ => None,
            };
            output.push('\u{2009}');
            i3_write_to(
                output,
                &format!("{:\u{2007}>2}", (t + 500) / 1000),
                icon,
                Some("°"),
                warn,
                icon_val,
            )
            .expect("Failed to write cpu temp");
        }
    }
}
