#![allow(clippy::cast_possible_truncation)]
#![allow(clippy::cast_sign_loss)]

use crate::constants::UPDATE_PERIOD;
use iirfilter::SimpleFirFilter;
use std::fs::File;
use std::marker::PhantomData;
use std::path::PathBuf;

use i3omnistatus::FileReader;

#[allow(clippy::empty_enum)]
pub enum Cpu {}
#[allow(clippy::empty_enum)]
pub enum Memory {}
#[allow(clippy::empty_enum)]
pub enum Io {}

pub trait Provider {
    fn get_file() -> PathBuf;
}

impl Provider for Cpu {
    fn get_file() -> PathBuf {
        "/proc/pressure/cpu".into()
    }
}

impl Provider for Memory {
    fn get_file() -> PathBuf {
        "/proc/pressure/memory".into()
    }
}

impl Provider for Io {
    fn get_file() -> PathBuf {
        "/proc/pressure/io".into()
    }
}

pub struct Pressure<KIND: Provider> {
    _kind: PhantomData<KIND>,
    buf: FileReader,
    last_some: u64,
    history: SimpleFirFilter<u8>,
}

impl<KIND: Provider> Pressure<KIND> {
    pub fn new() -> Option<Self> {
        let file = File::open(KIND::get_file()).ok()?;
        let buf = FileReader::with_capacities(file, 128, 128);

        Some(Self {
            _kind: PhantomData,
            buf,
            last_some: 0,
            history: SimpleFirFilter::new(),
        })
    }

    // Pressure averages sometimes mysteriously stop updating, so this method doesn't work right now
    pub fn get(&mut self) -> Option<(u8, u8)> {
        self.buf.rewind().ok()?;
        self.buf.read_all().ok()?;

        // #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
        let some_line = self.buf.get_str().lines().find(|l| l.starts_with("some"))?;
        let mut averages = some_line
            .split_whitespace()
            .filter(|c| c.starts_with("avg"))
            .map(|c| {
                c.split('=')
                    .nth(1)
                    .and_then(|s| s.split_once('.').map(|v| v.0))
                    .and_then(|s| s.parse::<u8>().ok())
                    .unwrap_or(100)
                    .clamp(0, 100)
            });
        #[allow(clippy::iter_nth_zero)]
        Some((
            averages.nth(0).unwrap_or(100),
            averages.nth(1).unwrap_or(100),
        ))
    }

    // pub fn get_history(&self) -> &SimpleFirFilter<u8> {
    //     &self.history
    // }

    // pub fn update(&mut self) -> Option<&Self> {
    //     self.buf.rewind().ok()?;
    //     self.buf.read_all().ok()?;

    //     let some_line = self.buf.get_str().lines().find(|l| l.starts_with("some"))?;
    //     let new = some_line
    //         .split_whitespace()
    //         .find_map(|s| s.strip_prefix("total="))?
    //         .parse::<u64>()
    //         .ok()?;
    //     let delta = new - self.last_some;
    //     self.last_some = new;
    //     let period = UPDATE_PERIOD.as_micros() as u64;
    //     let percent = ((100 * delta + period / 2) / period).clamp(0, 100);
    //     self.history.push(percent as u8);
    //     Some(self)
    // }
}
