#![allow(clippy::non_ascii_literal)]

use crate::blocks::{BlockOutputData, SyncBlockGenerator};
use formatter::{i3_write_to, WarnLevel};
use nix::sys::signal::{self, Signal};
use nix::unistd::Pid;
use std::fmt::Write;
use std::io::{BufRead, BufReader};
use std::process::{Command, Stdio};
use std::sync::{atomic::AtomicBool, atomic::AtomicI32, atomic::Ordering, Arc};
use std::thread::{self, Thread};
use std::{str, time};

fn get_volume() -> Option<i32> {
    let output = Command::new("pamixer")
        .arg("--get-volume")
        .output()
        .ok()?
        .stdout;
    let output = str::from_utf8(&output).ok()?.trim();
    let prcnt = output.parse::<u32>().ok()?;
    #[allow(clippy::cast_precision_loss)]
    let de_cubed = (prcnt as f32 / 100.0).powi(3);
    let db = 20.0 * de_cubed.log10();
    #[allow(clippy::cast_possible_truncation)]
    Some(db.round() as i32)
}

fn get_mute() -> bool {
    if let Ok(o) = Command::new("pamixer").arg("--get-mute").output() {
        let output = o.stdout;
        if let Ok(s) = str::from_utf8(&output) {
            return s.trim() == "true";
        }
    };
    false
}

fn write_volume<T: Write>(w: &mut T, v: Option<i32>) {
    match v {
        Some(v) => {
            let muted = get_mute();
            let icon = if muted || v <= -60 {
                '\u{f075f}'
            } else if v <= -40 {
                '\u{f057f}'
            } else if v <= -20 {
                '\u{f0580}'
            } else {
                '\u{f057e}'
            };
            i3_write_to(
                w,
                &format!("{v:\u{2007}>2}"),
                icon,
                Some("dB"),
                if muted || v > 0 {
                    WarnLevel::Warn
                } else {
                    WarnLevel::None
                },
                None,
            )
            .expect("Failed to write volume");
        }
        None => i3_write_to(w, "??", '\u{f0581}', Some("dB"), WarnLevel::Crit, None)
            .expect("Failed to write volume"),
    }
}

fn volume_thread(change_cb: &Arc<AtomicBool>, channel: &Arc<Thread>, pid: &Arc<AtomicI32>) {
    let mut pa_watcher = Command::new("pactl");
    pa_watcher.arg("subscribe");
    pa_watcher.stdout(Stdio::piped());

    loop {
        // Wait for volume readings to become valid.
        loop {
            let vol = get_volume();

            if vol.is_some() {
                change_cb.store(true, Ordering::Relaxed);
                channel.unpark();
                break;
            }
            thread::sleep(time::Duration::from_secs(4));
        }

        // Now subscribe to pactl to async watch for changes
        let mut child = pa_watcher.spawn().unwrap();
        #[allow(clippy::cast_possible_wrap)]
        pid.store(child.id() as i32, Ordering::Relaxed);
        let output = child.stdout.take().unwrap();

        let br = BufReader::new(output);

        for _ in br
            .lines()
            .map_while(Result::ok)
            .filter(|l| l.contains("change") && l.contains("sink"))
        {
            change_cb.store(true, Ordering::Relaxed);
            channel.unpark();
        }

        // If we got here, pactl has exited, so first reap the process
        let _ = child.wait();

        if pid.load(Ordering::Relaxed) == 0 {
            // Terminated child intentionally due to restart request, so just end thread
            return;
        }

        // Child terminated for external reasons, so wait a bit and try again from the top.
        thread::sleep(time::Duration::from_secs(4));
    }
}

pub struct Block {
    updated: Arc<AtomicBool>,
    child_pid: Arc<AtomicI32>,
    thread_guard: Option<thread::JoinHandle<()>>,
}

impl Block {
    pub fn new(channel: Arc<Thread>) -> Self {
        let updated = Arc::new(AtomicBool::new(true));
        let child_pid = Arc::new(AtomicI32::new(0));
        let updated_c = updated.clone();
        let child_c = child_pid.clone();
        let thread_guard = thread::Builder::new()
            .name("Volume".into())
            .spawn(move || volume_thread(&updated_c, &channel, &child_c))
            .unwrap();
        Self {
            updated,
            child_pid,
            thread_guard: Some(thread_guard),
        }
    }

    fn write_output(block: &mut BlockOutputData) {
        block.full_text.clear();
        write_volume(&mut block.full_text, get_volume());
    }

    pub fn close_child(&mut self) {
        let pid = self.child_pid.swap(0, Ordering::Relaxed);
        if pid != 0 {
            signal::kill(Pid::from_raw(pid), Signal::SIGTERM)
                .expect("Failed to terminate pactl child process");
        }
        if let Some(guard) = self.thread_guard.take() {
            let _ = guard.join();
        };
        // let _wait_status = waitpid(pid, None);
    }
}

impl SyncBlockGenerator for Block {
    fn update(&mut self, block: &mut BlockOutputData) -> bool {
        let result =
            self.updated
                .compare_exchange(true, false, Ordering::Relaxed, Ordering::Relaxed);
        if result.is_ok() {
            Self::write_output(block);
        }
        result.is_ok()
    }
}
